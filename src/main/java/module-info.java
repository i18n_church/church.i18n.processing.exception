module church.i18n.processing.exception {
  requires static org.jetbrains.annotations;
  requires transitive org.slf4j;

  exports church.i18n.processing.exception;
  exports church.i18n.processing.message;
  exports church.i18n.processing.storage;
  exports church.i18n.processing.logger;
  exports church.i18n.processing.config;
  exports church.i18n.processing.security.sanitizer;
  exports church.i18n.processing.security.policy;
  exports church.i18n.processing.status;
}