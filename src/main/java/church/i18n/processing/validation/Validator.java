/*
 * Copyright (c) 2024 Juraj Jurčo
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package church.i18n.processing.validation;

import static church.i18n.processing.validation.ValidatorNoopBuilder.NO_OP_VALIDATOR_BUILDER;

import java.util.function.Predicate;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

/**
 * A validation helper class that creates a fluid interface with construction of processing
 * messages.
 */
public class Validator {

  private Validator() {
    throw new IllegalStateException("Utility class");
  }

  /**
   * @param condition When a condition is met, it returns a builder for building processing message.
   *                  When the condition is false, it creates a no-operation builder and no
   *                  additional building operation is performed.
   * @return A processing message builder when the condition is met; no-operation builder otherwise.
   */
  public static @NotNull ValidatorBuilder when(final boolean condition) {
    return condition ? new ValidatorDefaultBuilder() : NO_OP_VALIDATOR_BUILDER;
  }

  /**
   * @param value     An object that is tested by the {@code condition}.
   * @param condition A condition that is evaluated over a {@code value} object.
   * @param <T>       A type of object that is tested.
   * @return A processing message builder when the condition is met; no-operation builder otherwise.
   */
  public static @NotNull <T> ValidatorBuilder when(final @Nullable T value,
      final @NotNull Predicate<T> condition) {
    return when(condition.test(value));
  }
}
