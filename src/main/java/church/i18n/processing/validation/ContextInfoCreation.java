/*
 * Copyright (c) 2024 Juraj Jurčo
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package church.i18n.processing.validation;

import church.i18n.processing.message.ContextInfo;
import church.i18n.processing.message.ContextValue;
import church.i18n.processing.message.I18nMessage;
import java.util.Optional;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

/**
 * Specification of context info creators.
 */
interface ContextInfoCreation {

  /**
   * Constructor with {@code name} and invalid {@code value}.
   *
   * @param name    Constructor with the only name of the parameter.
   * @param context An incorrect value that has caused the message to be produced.
   * @return An {@link Optional#of(Object)} holding value in the case it was built; {@link
   *     Optional#empty()} otherwise.
   */
  @NotNull
  Optional<ContextInfo> createContextInfo(@NotNull String name,
      @Nullable ContextValue context);


  /**
   * Constructor for full contextual info specification.
   *
   * @param name    Constructor with the only name of the parameter.
   * @param context An incorrect value that has caused the message to be produced.
   * @param message Additional explanatory message referring to the parameter. H.g.: "E-mail address
   *                'info@@i18n.church' contains invalid character at position [6]."
   * @return An {@link Optional#of(Object)} holding value in the case it was built; {@link
   *     Optional#empty()} otherwise.
   */
  @NotNull
  Optional<ContextInfo> createContextInfo(@NotNull String name,
      @Nullable ContextValue context, @Nullable I18nMessage message);

}
