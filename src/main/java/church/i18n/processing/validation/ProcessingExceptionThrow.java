/*
 * Copyright (c) 2024 Juraj Jurčo
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package church.i18n.processing.validation;

import church.i18n.processing.exception.ProcessingException;
import church.i18n.processing.message.ProcessingMessage;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

interface ProcessingExceptionThrow {

  /**
   * Throws exception with base code and message parameters.
   *
   * @param code   The {@code code} that may reference message in localization property file. When
   *               no such localized message with {@code code} has been found, the code itself is
   *               used as message.
   * @param params list of parameters needed for construction of localized message. If the last
   *               parameter is {@link Throwable}, it is used as cause of this exception.
   * @throws ProcessingException if the condition for building was {@code true}. Otherwise do
   *                             nothing.
   */
  void throwException(@NotNull String code, @Nullable Object... params);

  /**
   * Build exception with specified message.
   *
   * @param message Error message of the exception.
   * @throws ProcessingException if the condition for building was {@code true}. Otherwise do
   *                             nothing.
   */
  void throwException(@NotNull ProcessingMessage message);

  /**
   * Build exception with a message and concrete cause of this exception.
   *
   * @param message A message of the exception.
   * @param cause   The throwable that caused this {@link ProcessingException} to get thrown.
   * @throws ProcessingException if the condition for building was {@code true}. Otherwise do
   *                             nothing.
   */
  void throwException(@NotNull ProcessingMessage message, @NotNull Throwable cause);

  /**
   * Build exception with a message and concrete cause of this exception.
   *
   * @param exception An exception to throw.
   * @throws ProcessingException if the condition for building was {@code true}. Otherwise do
   *                             nothing.
   */
  void throwException(@NotNull ProcessingException exception);
}
