/*
 * Copyright (c) 2024 Juraj Jurčo
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package church.i18n.processing.validation;

import static church.i18n.processing.exception.ProcessingExceptionNoopBuilder.NO_OP_PROCESSING_EXCEPTION_BUILDER;
import static church.i18n.processing.message.ContextInfoNoopBuilder.NO_OP_CONTEXT_INFO_BUILDER;
import static church.i18n.processing.message.ProcessingMessageNoopBuilder.NO_OP_PROCESSING_MESSAGE_BUILDER;

import church.i18n.processing.exception.ProcessingException;
import church.i18n.processing.exception.ProcessingExceptionBuilder;
import church.i18n.processing.message.ContextInfo;
import church.i18n.processing.message.ContextInfoBuilder;
import church.i18n.processing.message.ContextValue;
import church.i18n.processing.message.I18nMessage;
import church.i18n.processing.message.ProcessingMessage;
import church.i18n.processing.message.ProcessingMessageBuilder;
import java.util.Optional;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

class ValidatorNoopBuilder implements ValidatorBuilder {

  public static final @NotNull ValidatorBuilder NO_OP_VALIDATOR_BUILDER = new ValidatorNoopBuilder();

  private ValidatorNoopBuilder() {
  }

  @Override
  public @NotNull ProcessingMessageBuilder<Optional<ProcessingMessage>> buildProcessingMessage(
      final @NotNull String code, final @Nullable Object... params) {
    return NO_OP_PROCESSING_MESSAGE_BUILDER;
  }

  @Override
  public @NotNull ProcessingMessageBuilder<Optional<ProcessingMessage>> buildProcessingMessage(
      final @NotNull I18nMessage message) {
    return NO_OP_PROCESSING_MESSAGE_BUILDER;
  }

  @Override
  public @NotNull ContextInfoBuilder buildContextInfo(final @NotNull String name) {
    return NO_OP_CONTEXT_INFO_BUILDER;
  }

  @Override
  public @NotNull ProcessingExceptionBuilder buildException(final @NotNull String code,
      final @Nullable Object... params) {
    return NO_OP_PROCESSING_EXCEPTION_BUILDER;
  }

  @Override
  public @NotNull ProcessingExceptionBuilder buildException(final @NotNull ProcessingMessage message) {
    return NO_OP_PROCESSING_EXCEPTION_BUILDER;
  }

  @Override
  public @NotNull ProcessingExceptionBuilder buildException(final @NotNull ProcessingMessage message,
      final @NotNull Throwable cause) {
    return NO_OP_PROCESSING_EXCEPTION_BUILDER;
  }

  @Override
  public @NotNull Optional<ContextInfo> createContextInfo(final @NotNull String name,
      final @Nullable ContextValue context) {
    return Optional.empty();
  }

  @Override
  public @NotNull Optional<ContextInfo> createContextInfo(final @NotNull String name,
      final @Nullable ContextValue context, final @Nullable I18nMessage message) {
    return Optional.empty();
  }

  @Override
  public @NotNull Optional<ProcessingException> createException(final @NotNull String code,
      final @Nullable Object... params) {
    return Optional.empty();
  }

  @Override
  public @NotNull Optional<ProcessingException> createException(final @NotNull ProcessingMessage message) {
    return Optional.empty();
  }

  @Override
  public @NotNull Optional<ProcessingException> createException(final @NotNull ProcessingMessage message,
      final @NotNull Throwable cause) {
    return Optional.empty();
  }

  @Override
  public @NotNull Optional<ProcessingMessage> createMessage(final @NotNull String code,
      final @Nullable Object... params) {
    return Optional.empty();
  }

  @Override
  public @NotNull Optional<ProcessingMessage> createMessage(final @NotNull I18nMessage message) {
    return Optional.empty();
  }

  @Override
  public void throwException(final @NotNull String code, final @Nullable Object... params) {
    //No exception to throw.
  }

  @Override
  public void throwException(final @NotNull ProcessingMessage message) {
    //No exception to throw.
  }

  @Override
  public void throwException(final @NotNull ProcessingMessage message,
      final @NotNull Throwable cause) {
    //No exception to throw.
  }

  @Override
  public void throwException(final @NotNull ProcessingException exception) {
    //No exception to throw.
  }

  @Override
  public @NotNull String toString() {
    return "ValidatorNoopBuilder{}";
  }
}
