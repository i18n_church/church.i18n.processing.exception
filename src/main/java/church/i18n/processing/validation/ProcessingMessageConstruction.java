/*
 * Copyright (c) 2024 Juraj Jurčo
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package church.i18n.processing.validation;

import church.i18n.processing.message.I18nMessage;
import church.i18n.processing.message.ProcessingMessage;
import church.i18n.processing.message.ProcessingMessageBuilder;
import java.util.Optional;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

/**
 * Non-terminal processing message builder.
 */
interface ProcessingMessageConstruction {

  /**
   * Processing message with additional information.
   *
   * @param code   The code reference in the localization property file.
   * @param params Parameters that needs to be provided to properly format the message with all.
   *               parameters.
   * @return An instance of this builder.
   */
  @NotNull
  ProcessingMessageBuilder<Optional<ProcessingMessage>> buildProcessingMessage(@NotNull String code,
      @Nullable Object... params);

  /**
   * Processing message builder from a localized message.
   *
   * @param message A localized message.
   * @return An instance of this builder.
   */
  @NotNull
  ProcessingMessageBuilder<Optional<ProcessingMessage>> buildProcessingMessage(
      @NotNull I18nMessage message);

}
