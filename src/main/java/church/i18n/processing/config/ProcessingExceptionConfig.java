/*
 * Copyright (c) 2024 Juraj Jurčo
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package church.i18n.processing.config;

import church.i18n.processing.message.MessageType;
import church.i18n.processing.message.ProcessingMessage;
import church.i18n.processing.security.policy.SecurityPolicy;
import java.util.Set;
import org.jetbrains.annotations.NotNull;

/**
 * Configuration of default behavior of library with
 */
public interface ProcessingExceptionConfig {

  /**
   * Specifies a default message type when no {@link MessageType} is specified within message.
   *
   * @return Default message type to use.
   */
  @NotNull
  MessageType defaultMessageType();

  /**
   * Specifies what default {@link SecurityPolicy} to use, when none is specified within message.
   *
   * @return Default {@link SecurityPolicy} to use.
   */
  @NotNull
  SecurityPolicy defaultSecurityPolicy();

  /**
   * Allowed/Safe to use {@link SecurityPolicy} in the application. All other should are considered
   * as unsafe and so cleaned out before exposing.
   *
   * @return A set of security levels that could be exposed.
   */
  @NotNull
  Set<SecurityPolicy> exposeSecurityPolicies();

  /**
   * A failover processing message that is used when there is not enough information provided and
   * the system has to return a message. This may happen when all information is sanitized or no
   * other information is available.
   *
   * @return A default message to use.
   */
  @NotNull
  ProcessingMessage failoverProcessingMessage();

  /**
   * Allowed/Safe to use {@link SecurityPolicy} in the application. All other should are considered
   * as unsafe and so cleaned out before logging.
   *
   * @return A set of security levels that could be logged.
   */
  @NotNull
  Set<SecurityPolicy> logSecurityPolicies();
}
