/*
 * Copyright (c) 2024 Juraj Jurčo
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package church.i18n.processing.config;

import static church.i18n.processing.message.MessageStatus.ERROR;
import static church.i18n.processing.security.policy.SecurityLevel.PUBLIC;
import static church.i18n.processing.security.policy.SecurityLevel.SYSTEM_EXTERNAL;
import static church.i18n.processing.security.policy.SecurityLevel.SYSTEM_INTERNAL;
import static church.i18n.processing.security.policy.SecurityLevel.THIRD_PARTY;

import church.i18n.processing.message.MessageType;
import church.i18n.processing.message.ProcessingMessage;
import church.i18n.processing.security.policy.SecurityPolicy;
import java.util.Set;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public record DefaultProcessingExceptionConfig(
    @NotNull MessageType defaultMessageType,
    @NotNull SecurityPolicy defaultSecurityPolicy,
    @NotNull Set<SecurityPolicy> exposeSecurityPolicies,
    @NotNull Set<SecurityPolicy> logSecurityPolicies,
    @NotNull ProcessingMessage failoverProcessingMessage
) implements ProcessingExceptionConfig {

  public static @NotNull Builder builder() {
    return new Builder();
  }

  @Override
  public boolean equals(final @Nullable Object o) {
    if (this == o) {
      return true;
    }
    if (!(o instanceof final DefaultProcessingExceptionConfig that)) {
      return false;
    }
    return this.failoverProcessingMessage.equals(that.failoverProcessingMessage)
        && this.exposeSecurityPolicies
        .equals(that.exposeSecurityPolicies) && this.logSecurityPolicies.equals(
        that.logSecurityPolicies)
        && this.defaultMessageType.equals(that.defaultMessageType) && this.defaultSecurityPolicy
        .equals(that.defaultSecurityPolicy);
  }

  @Override
  public @NotNull String toString() {
    return "DefaultProcessingExceptionConfig{" +
        "failoverProcessingMessage=" + this.failoverProcessingMessage +
        ", exposeSecurityPolicies=" + this.exposeSecurityPolicies +
        ", logSecurityPolicies=" + this.logSecurityPolicies +
        ", defaultMessageType=" + this.defaultMessageType +
        ", defaultSecurityPolicy=" + this.defaultSecurityPolicy +
        '}';
  }

  public static final class Builder {

    private @NotNull ProcessingMessage failoverProcessingMessage = new ProcessingMessage("err-g-1");
    private @NotNull Set<SecurityPolicy> exposeSecurityPolicies = Set.of(PUBLIC, SYSTEM_EXTERNAL);
    private @NotNull Set<SecurityPolicy> logSecurityPolicies = Set.of(PUBLIC, SYSTEM_EXTERNAL,
        SYSTEM_INTERNAL, THIRD_PARTY);
    private @NotNull MessageType defaultMessageType = ERROR;
    private @NotNull SecurityPolicy defaultSecurityPolicy = SYSTEM_INTERNAL;

    private Builder() {
    }

    public @NotNull Builder withFailoverProcessingMessage(
        final @NotNull ProcessingMessage failoverProcessingMessage) {
      this.failoverProcessingMessage = failoverProcessingMessage;
      return this;
    }

    public @NotNull Builder withExposeSecurityPolicies(
        final @NotNull Set<SecurityPolicy> exposeSecurityPolicies) {
      this.exposeSecurityPolicies = exposeSecurityPolicies;
      return this;
    }

    public @NotNull Builder withLogSecurityPolicies(final @NotNull Set<SecurityPolicy> logSecurityPolicies) {
      this.logSecurityPolicies = logSecurityPolicies;
      return this;
    }

    public @NotNull Builder withDefaultMessageType(final @NotNull MessageType defaultMessageType) {
      this.defaultMessageType = defaultMessageType;
      return this;
    }

    public @NotNull Builder withDefaultSecurityPolicy(final @NotNull SecurityPolicy defaultSecurityPolicy) {
      this.defaultSecurityPolicy = defaultSecurityPolicy;
      return this;
    }

    public @NotNull DefaultProcessingExceptionConfig build() {
      return new DefaultProcessingExceptionConfig(this.defaultMessageType,
          this.defaultSecurityPolicy, this.exposeSecurityPolicies, this.logSecurityPolicies,
          this.failoverProcessingMessage);
    }
  }
}
