/*
 * Copyright (c) 2024 Juraj Jurčo
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package church.i18n.processing.storage;

import church.i18n.processing.message.ProcessingMessage;
import java.util.List;
import org.jetbrains.annotations.NotNull;

/**
 * Retrieve messages from the storage.
 */
public interface StorageMessageProvider {

  /**
   * Clear message from the storage associated with {@code processingId}.
   *
   * @param processingId Identifier of messages associated with.
   */
  void clearStorage(@NotNull String processingId);

  /**
   * Retrieve all messages from the message storage without clearing them.
   *
   * @param processingId Identifier of messages associated with.
   * @return List of messages associated with the {@code processingId}.
   */
  @NotNull
  List<ProcessingMessage> get(@NotNull String processingId);

  /**
   * Retrieve all messages from storage and clear messages associated with the {@code
   * processingId}.
   *
   * @param processingId Identifier of messages associated with.
   * @return List of messages associated with {@code processingId}.
   */
  @NotNull
  List<ProcessingMessage> getAndClear(@NotNull String processingId);

}
