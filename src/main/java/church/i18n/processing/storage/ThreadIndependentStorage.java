/*
 * Copyright (c) 2024 Juraj Jurčo
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package church.i18n.processing.storage;

import church.i18n.processing.message.ProcessingMessage;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedDeque;
import org.jetbrains.annotations.NotNull;


/**
 * Thread independent storage of processing messages.
 */
public class ThreadIndependentStorage implements MessageStorage {

  private final @NotNull ConcurrentHashMap<String, ConcurrentLinkedDeque<ProcessingMessage>> messages =
      new ConcurrentHashMap<>();
  private final ProcessingIdProvider processingIdProvider;

  public ThreadIndependentStorage(final ProcessingIdProvider processingIdProvider) {
    this.processingIdProvider = processingIdProvider;
  }

  @Override
  public void addMessages(final @NotNull ProcessingMessage... messages) {
    addMessages(processingIdProvider.getProcessingId(), messages);
  }

  @Override
  public void addMessages(final @NotNull List<ProcessingMessage> messages) {
    addMessages(processingIdProvider.getProcessingId(), messages);
  }

  @Override
  public void clearStorage() {
    clearStorage(processingIdProvider.getProcessingId());
  }

  @Override
  public @NotNull List<ProcessingMessage> get() {
    return get(processingIdProvider.getProcessingId());
  }

  @Override
  public @NotNull List<ProcessingMessage> getAndClear() {
    return getAndClear(processingIdProvider.getProcessingId());
  }

  @Override
  public void addMessages(final @NotNull String processingId,
      final @NotNull ProcessingMessage... messages) {
    addMessages(processingId, List.of(messages));
  }

  @Override
  public void addMessages(final @NotNull String processingId,
      final @NotNull List<ProcessingMessage> messages) {
    this.messages
        .computeIfAbsent(processingId, k -> new ConcurrentLinkedDeque<>())
        .addAll(messages);
  }

  @Override
  public void clearStorage(final @NotNull String processingId) {
    this.messages.remove(processingId);
  }

  @Override
  public @NotNull List<ProcessingMessage> get(final @NotNull String processingId) {
    return this.messages
        .computeIfAbsent(processingId, k -> new ConcurrentLinkedDeque<>())
        .stream()
        .toList();
  }

  @Override
  public @NotNull List<ProcessingMessage> getAndClear(final @NotNull String processingId) {
    final ConcurrentLinkedDeque<ProcessingMessage> items = this.messages.remove(processingId);
    if (items != null) {
      return items
          .stream()
          .toList();
    }
    return List.of();
  }

  @Override
  public String toString() {
    return "ThreadIndependentStorage{" +
        "messages=" + messages +
        ", processingIdProvider=" + processingIdProvider +
        '}';
  }
}
