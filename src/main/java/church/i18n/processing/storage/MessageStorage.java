/*
 * Copyright (c) 2024 Juraj Jurčo
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package church.i18n.processing.storage;

import church.i18n.processing.message.ProcessingMessage;
import java.util.List;
import org.jetbrains.annotations.NotNull;

/**
 * Simplified message storage where implementing class takes care of providing a correct {@code
 * processingId}.
 */
public interface MessageStorage extends MessageStorageAppender, StorageMessageProvider {

  /**
   * Add messages into message storage.
   *
   * @param messages Messages to append.
   */
  void addMessages(@NotNull ProcessingMessage... messages);

  /**
   * Add messages into the thread independent storage.
   *
   * @param messages Messages to append.
   */
  void addMessages(@NotNull List<ProcessingMessage> messages);

  /**
   * Clear message from the storage.
   */
  void clearStorage();

  /**
   * Retrieve all messages from the message storage without clearing them.
   *
   * @return List of messages in a storage.
   */
  @NotNull
  List<ProcessingMessage> get();

  /**
   * Retrieve all messages from storage and clear messages associated with the {@code
   * processingId}.
   *
   * @return List of messages in a storage.
   */
  @NotNull
  List<ProcessingMessage> getAndClear();
}
