/*
 * Copyright (c) 2024 Juraj Jurčo
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package church.i18n.processing.exception;

import church.i18n.processing.logger.LogLevel;
import church.i18n.processing.message.ContextInfo;
import church.i18n.processing.message.MessageType;
import church.i18n.processing.message.ProcessingMessage;
import church.i18n.processing.security.policy.SecurityLevel;
import church.i18n.processing.status.DefaultStatus;
import church.i18n.processing.status.Status;
import java.io.Serial;
import java.net.URI;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

/**
 * Generic exception with enough information but not more for handling exceptional state in the
 * application. It holds information user may need to know what has happened, how to deal with the
 * problem or where to look for a solution.
 */
public class ProcessingException extends RuntimeException
    implements ProcessingInfo, ProcessingExceptionIntermediate<ProcessingException> {

  @Serial
  private static final long serialVersionUID = 1880343548762429830L;

  private @NotNull Status status = DefaultStatus.UNKNOWN;
  private @NotNull LogLevel logLevel = LogLevel.MESSAGE_TYPE_MAPPING;
  private @NotNull ProcessingMessage processingMessage;

  /**
   * Constructor with base code and message parameters.
   *
   * @param code   The {@code code} that may reference message in localization property file. When
   *               no such localized message with {@code code} has been found, the code itself is
   *               used as message.
   * @param params list of parameters needed for construction of localized message. If the last
   *               parameter is {@link Throwable}, it is used as cause of this exception.
   */
  public ProcessingException(final @NotNull String code, final @Nullable Object... params) {
    super(code);
    if (params == null) {
      this.processingMessage = new ProcessingMessage(code);
    } else if (params.length > 0 && params[params.length - 1] instanceof Throwable) {
      initCause((Throwable) params[params.length - 1]);
      Object[] newParams = Arrays.copyOf(params, params.length - 1);
      this.processingMessage = new ProcessingMessage(code, newParams);
    } else {
      this.processingMessage = new ProcessingMessage(code, params);
    }
  }

  /**
   * Constructor with specified message.
   *
   * @param message Error message of the exception.
   */
  public ProcessingException(final @NotNull ProcessingMessage message) {
    super(message.getMessage().getCode());
    this.processingMessage = message;
  }

  /**
   * Constructor with a message and concrete cause of this exception.
   *
   * @param message A message of the exception.
   * @param cause   The throwable that caused this {@link ProcessingException} to get thrown.
   */
  public ProcessingException(final @NotNull ProcessingMessage message,
      final @Nullable Throwable cause) {
    this(message);
    if (cause != null) {
      initCause(cause);
      setStackTrace(cause.getStackTrace());
    }
  }

  @Override
  public @NotNull ProcessingException addContextInfo(final @Nullable List<ContextInfo> contextInfo) {
    this.processingMessage = ProcessingMessage
        .withMessage(this.processingMessage)
        .addContextInfo(contextInfo)
        .build();
    return this;
  }

  @Override
  public @NotNull ProcessingException addContextInfo(final @Nullable ContextInfo... contextInfo) {
    this.processingMessage = ProcessingMessage
        .withMessage(this.processingMessage)
        .addContextInfo(contextInfo)
        .build();
    return this;
  }

  @Override
  public @NotNull ProcessingException withHelpUri(final @Nullable URI helpUri) {
    this.processingMessage = ProcessingMessage
        .withMessage(this.processingMessage)
        .withHelpUri(helpUri)
        .build();
    return this;
  }

  @Override
  public @NotNull ProcessingException withHelpUri(final @NotNull String helpUri) {
    this.processingMessage = ProcessingMessage
        .withMessage(this.processingMessage)
        .withHelpUri(helpUri)
        .build();
    return this;
  }

  @Override
  public @NotNull ProcessingException withSecurityLevel(final @Nullable SecurityLevel securityLevel) {
    this.processingMessage = ProcessingMessage
        .withMessage(this.processingMessage)
        .withSecurityLevel(securityLevel)
        .build();
    return this;
  }

  @Override
  public @NotNull ProcessingException withMessageType(final @NotNull MessageType messageType) {
    this.processingMessage = ProcessingMessage
        .withMessage(this.processingMessage)
        .withMessageType(messageType)
        .build();
    return this;
  }

  @Override
  public @NotNull LogLevel getLogLevel() {
    return this.logLevel;
  }

  @Override
  public @NotNull ProcessingMessage getProcessingMessage() {
    return this.processingMessage;
  }

  @Override
  public @NotNull Status getStatus() {
    return this.status;
  }

  @Override
  public @NotNull ProcessingException withLogLevel(final @NotNull LogLevel logLevel) {
    this.logLevel = logLevel;
    return this;
  }

  @Override
  public @NotNull ProcessingException withStatus(final @NotNull Status status) {
    this.status = status;
    return this;
  }

  @Override
  public int hashCode() {
    return Objects.hash(this.status, this.logLevel, this.processingMessage);
  }

  @Override
  public boolean equals(final Object obj) {
    if (this == obj) {
      return true;
    }
    if (!(obj instanceof final ProcessingException that)) {
      return false;
    }
    return this.status.equals(that.status)
        && this.logLevel == that.logLevel
        && Objects.equals(this.processingMessage, that.processingMessage);
  }

  @Override
  public @NotNull String toString() {
    return "ProcessingException{" +
        "status=" + this.status +
        ", logLevel=" + this.logLevel +
        ", processingMessage=" + this.processingMessage +
        "} ";
  }
}
