/*
 * Copyright (c) 2024 Juraj Jurčo
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package church.i18n.processing.exception;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public final class ExceptionUtils {

  private ExceptionUtils() {
    throw new IllegalStateException("Utility class");
  }

  /**
   * It may happen that external libraries wrap our exception into different exception. Method finds the first
   * {@link ProcessingException} that is wrapped in the original exception cause.
   *
   * @param original Original exception to check.
   * @return Original wrapped {@link ProcessingException} that was originally thrown. When there is no such exception it
   *     returns {@link Optional#empty()}.
   */
  public static @NotNull Optional<ProcessingException> getWrappedException(
      final @Nullable Throwable original) {
    if (original == null) {
      return Optional.empty();
    }
    Throwable cause = original.getCause();
    //Avoid recursive calls; set of memory references.
    Set<Integer> causes = new HashSet<>();
    causes.add(System.identityHashCode(original));
    int causeId = System.identityHashCode(cause);
    while (cause != null && !(cause instanceof ProcessingException) && !causes
        .contains(causeId)) {
      causes.add(causeId);
      cause = cause.getCause();
      causeId = System.identityHashCode(cause);
    }
    if (causes.contains(causeId)) {
      return Optional.empty();
    }
    if (cause instanceof final ProcessingException processingException) {
      return Optional.of(processingException);
    }
    return Optional.empty();
  }

  /**
   * Create a new instance of {@link ProcessingException} with different code and places the original exception as the
   * cause of newly created exception.
   *
   * @param original  Original exception.
   * @param newCode   New mapping code for new exception.
   * @param newParams Params of the new exception message.
   * @return New instance of mapped exception.
   */
  public static @NotNull ProcessingException wrap(final @NotNull ProcessingException original,
      final @NotNull String newCode, final @Nullable Object... newParams) {
    return new ProcessingException(newCode, appendCause(original.getCause(), newParams));
  }

  /**
   * Finds inner {@link ProcessingException} and remaps it into a new {@link ProcessingException} with a new code.
   *
   * @param original  Original thrown exception.
   * @param newCode   New mapping code.
   * @param newParams Parameters for the original mapping.
   * @return Newly mapped exception in the case there was inner {@link ProcessingException} cause, otherwise it creates
   *     a new instance of {@link ProcessingException} with the cause of {@code original} exception.
   */
  public static @NotNull ProcessingException rewrapWrapped(final @NotNull Throwable original,
      final @NotNull String newCode, final @Nullable Object... newParams) {
    Optional<ProcessingException> wrappedException = getWrappedException(original);
    return wrappedException
        .map(e -> wrap(e, newCode, newParams))
        .orElseGet(
            () -> new ProcessingException(newCode, appendCause(original.getCause(), newParams))
        );
  }

  @SafeVarargs
  public static Optional<ProcessingException> tryTranslate(
      final @NotNull Exception e,
      final @NotNull ExceptionTranslator<Exception>... translators) {
    if (translators.length == 0) {
      return Optional.empty();
    }
    return Arrays.stream(translators)
        .map(translator -> translator.translate(e))
        .filter(Optional::isPresent)
        .map(Optional::get)
        .findFirst();
  }

  /**
   * Append the cause at the end of array of parameters.
   *
   * @param cause  The cause to be appended.
   * @param params Array of parameters.
   * @return Array of single size if params is {@code null} or empty. If the cause is {@code null} {@code params} are
   *     returned. In the case both params and cause are {@code null}, it returns empty array. Otherwise, it returns the
   *     params array with appended cause at the end.
   */
  private static @NotNull Object[] appendCause(
      final @Nullable Throwable cause,
      final @Nullable Object... params) {
    if (params == null) {
      return cause == null ? new Object[0] : new Object[]{cause};
    }
    if (cause == null) {
      //noinspection NullableProblems
      return params;
    } else {
      Object[] newParams = Arrays.copyOf(params, params.length + 1);
      newParams[params.length] = cause;
      return newParams;
    }
  }
}
