/*
 * Copyright (c) 2024 Juraj Jurčo
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package church.i18n.processing.exception;

import java.util.Optional;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

/**
 * Common interface for writing mappers of (framework) exceptions. Many times we encounter common
 * framework messages that needs to be parsed to extract information from them. Frameworks are used
 * repeatedly in multiple projects and handling those exceptions is most of the time the same. This
 * interface should unify handling of such cases and reuse once written implementation. Example of
 * such implementation could be SqlException of particular database driver or common network-related
 * exceptions.
 *
 * @param <T> Type of exception it handles. E.g. SqlException
 */
public interface ExceptionTranslator<T extends Exception> {

  /**
   * Parse framework or other repeated exception into a {@link ProcessingException}. Please extract
   * as much information as needed to properly understand error state but no more than that. Beware
   * of security concerns and do not provide any sensitive information. Please do not return
   * directly {@link Exception#getMessage()} or {@link Exception#getLocalizedMessage()} without
   * parsing its content and selecting only information you trust. Never trust messages from third
   * party libraries, you do not have a control over them. One version may return correct message,
   * later version may return sensitive information. When you implement this interface, try to write
   * as many test cases as you can to validate particular exception mapping. When library version
   * for which you write exception translator changes, you may need to validate that messages has
   * not changed and mapping has still the same behaviour.
   *
   * @param exception An exception to parse.
   * @return Optional with instance of {@link ProcessingException} in the case this mapper is able
   *     to properly parse it, {@link Optional#empty()} otherwise.
   */
  @NotNull
  Optional<ProcessingException> translate(@Nullable T exception);

  /**
   * Translate the exception with the translator. In the case the translator is capable to handle
   * such exception, translated exception will be thrown, otherwise no action is performed.
   *
   * @param exception Exception to translate.
   */
  default void translateAndThrow(final @Nullable T exception) {
    Optional<ProcessingException> translatedException = translate(exception);
    if (translatedException.isPresent()) {
      throw translatedException.get();
    }
  }

}
