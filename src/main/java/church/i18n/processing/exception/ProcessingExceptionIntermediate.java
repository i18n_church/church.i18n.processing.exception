/*
 * Copyright (c) 2024 Juraj Jurčo
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package church.i18n.processing.exception;

import church.i18n.processing.logger.LogLevel;
import church.i18n.processing.message.ProcessingMessageBuilderMethods;
import church.i18n.processing.status.Status;
import org.jetbrains.annotations.NotNull;

public interface ProcessingExceptionIntermediate<T extends ProcessingMessageBuilderMethods<T>>
    extends ProcessingMessageBuilderMethods<T> {

  /**
   * Specify log level of exception.
   *
   * @param logLevel The log level you want to use.
   * @return An instance of object {@link T}.
   */
  @NotNull
  T withLogLevel(@NotNull LogLevel logLevel);

  /**
   * Specify the response status of an exception that can be used for system inter-communication.
   *
   * @param status Status associated with this exception.
   * @return An instance of object {@link T}.
   */
  @NotNull
  T withStatus(@NotNull Status status);

}
