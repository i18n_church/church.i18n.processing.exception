/*
 * Copyright (c) 2024 Juraj Jurčo
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package church.i18n.processing.exception;

import church.i18n.processing.logger.LogLevel;
import church.i18n.processing.message.ContextInfo;
import church.i18n.processing.message.MessageType;
import church.i18n.processing.message.ProcessingMessageBuilderMethods;
import church.i18n.processing.security.policy.SecurityLevel;
import church.i18n.processing.status.Status;
import java.net.URI;
import java.util.List;
import java.util.Optional;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

/**
 * No operation builder. This instance is used to reduce the number of operations used during
 * building. In the case validation condition fails, the building of an exception is not needed any
 * more. In this case we can skip all following building operations.
 */
public class ProcessingExceptionNoopBuilder
    implements ProcessingMessageBuilderMethods<ProcessingExceptionBuilder>,
    ProcessingExceptionBuilder {

  public static final @NotNull ProcessingExceptionBuilder NO_OP_PROCESSING_EXCEPTION_BUILDER =
      new ProcessingExceptionNoopBuilder();

  private ProcessingExceptionNoopBuilder() {
    //Hiding constructor to enforce usage of static constant.
  }

  @Override
  public @NotNull ProcessingExceptionBuilder addContextInfo(final @Nullable List<ContextInfo> contextInfo) {
    return this;
  }

  @Override
  public @NotNull ProcessingExceptionBuilder addContextInfo(final @Nullable ContextInfo... contextInfo) {
    return this;
  }

  @Override
  public @NotNull ProcessingExceptionBuilder withHelpUri(final @Nullable URI helpUri) {
    return this;
  }

  @Override
  public @NotNull ProcessingExceptionBuilder withHelpUri(final @NotNull String helpUri) {
    return this;
  }

  @Override
  public @NotNull ProcessingExceptionBuilder withSecurityLevel(final @Nullable SecurityLevel securityLevel) {
    return this;
  }

  @Override
  public @NotNull ProcessingExceptionBuilder withMessageType(final @Nullable MessageType messageType) {
    return this;
  }

  @Override
  public @NotNull Optional<ProcessingException> build() {
    return Optional.empty();
  }

  @Override
  public void throwException() {
    //Do nothing.
  }

  @Override
  public @NotNull ProcessingExceptionBuilder withLogLevel(final @NotNull LogLevel logLevel) {
    return this;
  }

  @Override
  public @NotNull ProcessingExceptionBuilder withStatus(final @NotNull Status status) {
    return this;
  }

  @Override
  public @NotNull String toString() {
    return "ProcessingExceptionNoopBuilder{}";
  }
}
