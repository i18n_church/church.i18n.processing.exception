/*
 * Copyright (c) 2024 Juraj Jurčo
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package church.i18n.processing.exception;

import church.i18n.processing.logger.LogLevel;
import church.i18n.processing.message.I18nMessage;
import church.i18n.processing.message.ProcessingMessage;
import church.i18n.processing.message.ProcessingMessageAbstractBuilder;
import church.i18n.processing.status.Status;
import java.util.Objects;
import java.util.Optional;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

/**
 * Exception builder that builds actual exception. When exception is build, it returns non-empty
 * optional object with exception.
 */
public class ProcessingExceptionOptionalBuilder
    extends ProcessingMessageAbstractBuilder<ProcessingExceptionBuilder>
    implements ProcessingExceptionBuilder {

  private Status status;
  private LogLevel logLevel;
  private Throwable cause;

  public ProcessingExceptionOptionalBuilder(final @NotNull String code,
      final @Nullable Object... params) {
    super(code, params);
  }

  public ProcessingExceptionOptionalBuilder(final @NotNull I18nMessage message) {
    super(message);
  }

  public ProcessingExceptionOptionalBuilder(final @NotNull ProcessingMessage message) {
    super(message);
  }

  public ProcessingExceptionOptionalBuilder(final @NotNull ProcessingMessage message,
      final @NotNull Throwable cause) {
    super(message);
    this.cause = cause;
  }

  @Override
  public @NotNull Optional<ProcessingException> build() {
    return Optional.of(buildException());
  }

  @Override
  public void throwException() {
    throw buildException();
  }

  @Override
  public @NotNull ProcessingExceptionOptionalBuilder withLogLevel(final @NotNull LogLevel logLevel) {
    this.logLevel = logLevel;
    return this;
  }

  @Override
  public @NotNull ProcessingExceptionOptionalBuilder withStatus(final @NotNull Status status) {
    this.status = status;
    return this;
  }

  private @NotNull ProcessingException buildException() {
    ProcessingMessage pm = new ProcessingMessage(this.message, this.helpUri, this.messageType,
        this.contextInfo, this.securityLevel);
    ProcessingException exception = this.cause == null
                                    ? new ProcessingException(pm)
                                    : new ProcessingException(pm, this.cause);
    if (this.logLevel != null) {
      exception.withLogLevel(this.logLevel);
    }
    if (this.status != null) {
      exception.withStatus(this.status);
    }
    return exception;
  }

  @Override
  public int hashCode() {
    return Objects.hash(super.hashCode(), this.status, this.logLevel, this.cause);
  }

  @Override
  public boolean equals(final @Nullable Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    if (!super.equals(o)) {
      return false;
    }
    ProcessingExceptionOptionalBuilder that = (ProcessingExceptionOptionalBuilder) o;
    return Objects.equals(this.status, that.status) &&
        this.logLevel == that.logLevel &&
        Objects.equals(this.cause, that.cause);
  }

  @Override
  public @NotNull String toString() {
    return "ProcessingExceptionOptionalBuilder{" +
        "status=" + this.status +
        ", logLevel=" + this.logLevel +
        ", cause=" + this.cause +
        ", message=" + this.message +
        ", contextInfo=" + this.contextInfo +
        ", helpUri=" + this.helpUri +
        ", messageType=" + this.messageType +
        ", securityLevel=" + this.securityLevel +
        "} " + super.toString();
  }
}
