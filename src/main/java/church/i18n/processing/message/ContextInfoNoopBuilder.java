/*
 * Copyright (c) 2024 Juraj Jurčo
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package church.i18n.processing.message;

import church.i18n.processing.security.policy.SecurityLevel;
import java.io.Serializable;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class ContextInfoNoopBuilder implements ContextInfoBuilder {

  public static final @NotNull ContextInfoBuilder NO_OP_CONTEXT_INFO_BUILDER = new ContextInfoNoopBuilder();

  private ContextInfoNoopBuilder() {
  }

  @Override
  public @Nullable ContextInfo build() {
    return null;
  }

  @Override
  public @NotNull ContextInfoBuilder withContext(final @Nullable Serializable value,
      final @Nullable ValueType valueType) {
    return this;
  }

  @Override
  public @NotNull ContextInfoBuilder withContext(final @Nullable Serializable value,
      final @Nullable String valueType) {
    return this;
  }

  @Override
  public @NotNull ContextInfoBuilder withContext(final @Nullable Serializable value) {
    return this;
  }

  @Override
  public @NotNull ContextInfoBuilder withContext(final @NotNull ContextValue context) {
    return this;
  }

  @Override
  public @NotNull ContextInfoBuilder withHelp(final @Nullable ContextValue help) {
    return this;
  }

  @Override
  public @NotNull ContextInfoBuilder withHelp(final @Nullable Serializable help,
      final @Nullable ValueType helpType) {
    return this;
  }

  @Override
  public @NotNull ContextInfoBuilder withHelp(final @Nullable Serializable help,
      final @Nullable String helpType) {
    return this;
  }

  @Override
  public @NotNull ContextInfoBuilder withHelp(final @Nullable Serializable help) {
    return this;
  }

  @Override
  public @NotNull ContextInfoBuilder withMessage(final @Nullable I18nMessage message) {
    return this;
  }

  @Override
  public @NotNull ContextInfoBuilder withMessage(final @Nullable String code,
      final @Nullable Object... params) {
    return this;
  }

  @Override
  public @NotNull ContextInfoBuilder withSecurityLevel(
      final @Nullable SecurityLevel securityLevel) {
    return this;
  }

  @Override
  public @NotNull String toString() {
    return "ContextInfoNoopBuilder{}";
  }
}
