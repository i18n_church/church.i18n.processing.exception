/*
 * Copyright (c) 2024 Juraj Jurčo
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package church.i18n.processing.message;

import church.i18n.processing.security.policy.SecurityLevel;
import church.i18n.processing.security.policy.SecurityPolicy;
import java.io.Serial;
import java.io.Serializable;
import java.util.Objects;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

/**
 * Holder of contextual info of the {@link ProcessingMessage}. When the message is constructed, the
 * place where it occurs is usually the place with the most contextual information that can be added
 * to the message. Such information could be useful either for user either for machine processing.
 */
public class ContextInfo implements Serializable {

  @Serial
  private static final long serialVersionUID = -24575290638716939L;
  private final @NotNull String name;
  private final @Nullable ContextValue context;
  private final @Nullable ContextValue help;
  private final @Nullable I18nMessage message;
  private final @Nullable SecurityLevel securityLevel;

  /**
   * Constructor for contextual information associated with message.
   *
   * @param name          Name or identification of an contextual information. Should be unique per
   *                      message. E.g. contains the name of invalid property value.
   * @param context       Contextual information associated with the message. May contain e.g.
   *                      invalid value from validation and it's type.
   * @param help          Help information associated with invalid value. May contain e.g. regular
   *                      expression that value needs to meet; value range information; or so.
   * @param message       Human-readable message describing the problem associated with the context
   *                      info. E.g.: "E-mail address 'info@@i18n.church' contains context character
   *                      at position [6]."
   * @param securityLevel The sensitivity level of this contextual information.
   */
  public ContextInfo(
      final @NotNull String name,
      final @Nullable ContextValue context,
      final @Nullable ContextValue help,
      final @Nullable I18nMessage message,
      final @Nullable SecurityLevel securityLevel) {
    this.name = name;
    this.context = context;
    this.help = help;
    this.message = message;
    this.securityLevel = securityLevel;
  }

  /**
   * Creates a new {@link ContextInfoBuilder} by copying values from existing instance.
   *
   * @param copy Context info to copy.
   * @return A new {@link ContextInfoBuilder} with copied values.
   */
  public static @NotNull ContextInfoBuilder builder(final @NotNull ContextInfo copy) {
    return new ContextInfoDefaultBuilder(copy.name)
        .withContext(copy.context)
        .withHelp(copy.help)
        .withMessage(copy.message)
        .withSecurityLevel(copy.securityLevel);
  }

  /**
   * New instance creator with only the name and contextual information.
   *
   * @param name    Name or identification of an contextual information. Should be unique per
   *                message. E.g. contains the name of invalid property value.
   * @param context Contextual information associated with the message. May contain e.g. invalid
   *                value from validation and it's type.
   * @return A new {@link ContextInfo} instance with name and contextual information only.
   */
  public static @Nullable ContextInfo of(final @NotNull String name, final @Nullable ContextValue context) {
    return new ContextInfoDefaultBuilder(name).withContext(context).build();
  }


  /**
   * New instance creator with the name, contextual information and message.
   *
   * @param name    Name or identification of an contextual information. Should be unique per
   *                message. E.g. contains the name of invalid property value.
   * @param context Contextual information associated with the message. May contain e.g. invalid
   *                value from validation and it's type.
   * @param message Human-readable message describing the problem associated with the context info.
   *                E.g.: "E-mail address 'info@@i18n.church' contains context character at position
   *                [6]."
   * @return A new {@link ContextInfo} instance with name, contextual information and message.
   */
  public static @NotNull ContextInfo of(final @NotNull String name, final @Nullable ContextValue context,
      final @Nullable I18nMessage message) {
    //noinspection ConstantConditions
    return new ContextInfoDefaultBuilder(name).withContext(context).withMessage(message).build();
  }

  /**
   * Constructor for full contextual info specification.
   *
   * @param name    Name or identification of contextual information. Should be unique per
   *                message. E.g. contains the name of invalid property value.
   * @param context Contextual information associated with the message. May contain e.g. invalid
   *                value from validation and it's type.
   * @param help    Help information associated with invalid value. May contain e.g. regular
   *                expression that value needs to meet; value range information; or so.
   * @param message Human-readable message describing the problem associated with the context info.
   *                E.g.: "E-mail address 'info@@i18n.church' contains context character at position
   *                [6]."
   * @return A new {@link ContextInfo} instance with name, message, context and help information.
   */
  public static @NotNull ContextInfo of(final @NotNull String name, final @Nullable ContextValue context,
      final @Nullable ContextValue help, final @Nullable I18nMessage message) {
    //noinspection ConstantConditions
    return new ContextInfoDefaultBuilder(name).withContext(context).withHelp(help)
        .withMessage(message).build();
  }

  /**
   * Contextual info builder with minimal required information - the name of contextual
   * information.
   *
   * @param name Name or identification of contextual information. Should be unique per message.
   *             E.g. contains the name of invalid property value.
   * @return A new {@link ContextInfoBuilder} with name.
   */
  public static @NotNull ContextInfoBuilder of(final @NotNull String name) {
    return new ContextInfoDefaultBuilder(name);
  }

  public @Nullable ContextValue getContext() {
    return this.context;
  }

  public @Nullable ContextValue getHelp() {
    return this.help;
  }

  public @Nullable I18nMessage getMessage() {
    return this.message;
  }

  public @NotNull String getName() {
    return this.name;
  }

  public @Nullable SecurityPolicy getSecurityLevel() {
    return this.securityLevel;
  }

  @Override
  public int hashCode() {
    return Objects.hash(this.name, this.context, this.help, this.message, this.securityLevel);
  }

  @Override
  public boolean equals(final Object o) {
    if (this == o) {
      return true;
    }
    if (!(o instanceof final ContextInfo that)) {
      return false;
    }
    return this.name.equals(that.name)
        && Objects.equals(this.context, that.context)
        && Objects.equals(this.help, that.help)
        && Objects.equals(this.message, that.message)
        && Objects.equals(this.securityLevel, that.securityLevel);
  }

  @Override
  public @NotNull String toString() {
    return "ContextInfo{"
        + "name='" + this.name + '\''
        + ", context=" + this.context
        + ", help=" + this.help
        + ", message=" + this.message
        + ", securityLevel=" + this.securityLevel
        + '}';
  }
}
