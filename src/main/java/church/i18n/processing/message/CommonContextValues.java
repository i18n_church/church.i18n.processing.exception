/*
 * Copyright (c) 2024 Juraj Jurčo
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package church.i18n.processing.message;

import static church.i18n.processing.message.ValueType.DATETIME;
import static church.i18n.processing.message.ValueType.REGEXP;

import org.jetbrains.annotations.NotNull;

/**
 * Lists the common context values for a quicker reference in the code.
 */
public interface CommonContextValues {

  @NotNull
  ContextValue INTEGER_REGEX = new ContextValue("\\d+", REGEXP);
  @NotNull
  ContextValue DOUBLE_REGEX = new ContextValue("[+-]?\\d*\\.?\\d+", REGEXP);
  @NotNull
  ContextValue DATE = new ContextValue("YYYY-MM-DD", DATETIME);
  @NotNull
  ContextValue TIME = new ContextValue("hh:mm:ss.s", DATETIME);
  @NotNull
  ContextValue ISO_ZONED_DATE_TIME = new ContextValue("YYYY-MM-DDThh:mm:ss.sTZD", DATETIME);
  @NotNull
  ContextValue SIMPLE_EMAIL_REGEX = new ContextValue("^\\w+@[a-zA-Z_]+?\\.[a-zA-Z]{2,3}$", REGEXP);

}
