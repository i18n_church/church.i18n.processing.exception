/*
 * Copyright (c) 2024 Juraj Jurčo
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package church.i18n.processing.message;

import church.i18n.processing.security.policy.SecurityLevel;
import church.i18n.processing.storage.MessageStorage;
import java.net.URI;
import java.util.List;
import java.util.Optional;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

/**
 * A processing message builder that only implements interface and does not do any action on
 * provided parameters. This is used for {@link church.i18n.processing.validation.Validator} when
 * provided condition is not met, the building of an exception does not make sense any more. The
 * build method returns {@link Optional#empty()} and other terminal methods has no side effects.
 */
public class ProcessingMessageNoopBuilder implements
    ProcessingMessageBuilder<Optional<ProcessingMessage>>,
    ProcessingMessageBuilderMethods<ProcessingMessageBuilder<Optional<ProcessingMessage>>> {

  public static final @NotNull ProcessingMessageBuilder<Optional<ProcessingMessage>>
      NO_OP_PROCESSING_MESSAGE_BUILDER = new ProcessingMessageNoopBuilder();

  private ProcessingMessageNoopBuilder() {
    //Hiding constructor to enforce usage of static constant.
  }

  @Override
  public void addToMessageStorage(final @NotNull MessageStorage storage) {
    //Do nothing.
  }

  @Override
  public @NotNull ProcessingMessageBuilder<Optional<ProcessingMessage>> addContextInfo(
      final @Nullable List<ContextInfo> contextInfo) {
    return this;
  }

  @Override
  public @NotNull ProcessingMessageBuilder<Optional<ProcessingMessage>> addContextInfo(
      final @Nullable ContextInfo... contextInfo) {
    return this;
  }

  @Override
  public @NotNull ProcessingMessageBuilder<Optional<ProcessingMessage>> withHelpUri(
      final @Nullable URI helpUri) {
    return this;
  }

  @Override
  public @NotNull ProcessingMessageBuilder<Optional<ProcessingMessage>> withHelpUri(
      final @NotNull String helpUri) {
    return this;
  }

  @Override
  public @NotNull ProcessingMessageBuilder<Optional<ProcessingMessage>> withSecurityLevel(
      final @Nullable SecurityLevel securityLevel) {
    return this;
  }

  @Override
  public @NotNull ProcessingMessageBuilder<Optional<ProcessingMessage>> withMessageType(
      final @Nullable MessageType messageType) {
    return this;
  }

  @Override
  public @NotNull Optional<ProcessingMessage> build() {
    return Optional.empty();
  }

  @Override
  public @NotNull String toString() {
    return "ProcessingMessageNoopBuilder{}";
  }
}
