/*
 * Copyright (c) 2024 Juraj Jurčo
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package church.i18n.processing.message;

import java.io.Serial;
import java.io.Serializable;
import java.util.Arrays;
import java.util.Objects;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

/**
 * Message specified by its code that could be a reference to resource bundle; or a raw message
 * that could contain parameters. The message could be localized by {@link java.util.ResourceBundle}
 * and formatted by {@link java.text.MessageFormat}.
 */
public class I18nMessage implements Serializable {

  @Serial
  private static final long serialVersionUID = 6505163176616746180L;
  private final @NotNull String code;
  private final @Nullable Object[] messageParams;

  /**
   * Simple message that can be localized and formatted.
   *
   * @param code   The code reference in the localization property file; or a raw message that could
   *               contain parameters.
   * @param params Parameters that needs to be provided to properly format the message with all
   *               parameters.
   */
  public I18nMessage(final @NotNull String code, final @Nullable Object... params) {
    this.code = code;
    this.messageParams = Objects.requireNonNullElseGet(params, () -> new Object[]{});
  }

  /**
   * The code reference in the localization property file; or a raw message that could contain
   * parameters.
   *
   * @return Assigned code value.
   */
  public @NotNull String getCode() {
    return this.code;
  }

  /**
   * Parameters that needs to be provided to properly format the message with all parameters.
   *
   * @return The array of assigned parameters.
   */
  public @Nullable Object[] getMessageParams() {
    return this.messageParams;
  }

  @Override
  public int hashCode() {
    int result = Objects.hash(this.code);
    result = 31 * result + Arrays.hashCode(this.messageParams);
    return result;
  }

  @Override
  public boolean equals(final Object obj) {
    if (this == obj) {
      return true;
    }
    if (!(obj instanceof final I18nMessage that)) {
      return false;
    }
    return Objects.equals(this.code, that.code)
        && Arrays.deepEquals(this.messageParams, that.messageParams);
  }

  @Override
  public @NotNull String toString() {
    return "I18nMessage{"
        + "code='" + this.code + '\''
        + ", messageParams=" + Arrays.toString(this.messageParams)
        + '}';
  }
}
