/*
 * Copyright (c) 2024 Juraj Jurčo
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package church.i18n.processing.message;

import church.i18n.processing.storage.MessageStorage;
import java.util.Optional;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

/**
 * Processing message builder implementation. The builder method returns non-empty {@link Optional}
 * with an instance of built {@link ProcessingMessage}.
 */
public class ProcessingMessageOptionalBuilder
    extends ProcessingMessageAbstractBuilder<ProcessingMessageBuilder<Optional<ProcessingMessage>>>
    implements ProcessingMessageBuilder<Optional<ProcessingMessage>> {

  /**
   * Build a processing message from code and it's message parameters.
   *
   * @param code   The code reference in the localization property file; or a raw message that could
   *               contain parameters.
   * @param params Parameters that needs to be provided to properly format the message with all
   *               parameters.
   * @see I18nMessage#I18nMessage(String, Object...)
   */
  public ProcessingMessageOptionalBuilder(final @NotNull String code,
      final @Nullable Object... params) {
    super(code, params);
  }

  /**
   * Build a processing message from provided message.
   *
   * @param message Raw unformatted response message.
   */
  public ProcessingMessageOptionalBuilder(final @NotNull I18nMessage message) {
    super(message);
  }

  /**
   * Copy constructor for the {@link ProcessingMessage} object.
   *
   * @param processingMessage A processing message to copy.
   */
  public ProcessingMessageOptionalBuilder(final @NotNull ProcessingMessage processingMessage) {
    super(processingMessage);
  }

  @Override
  public void addToMessageStorage(final @NotNull MessageStorage storage) {
    build().ifPresent(storage::addMessages);
  }

  @Override
  public @NotNull Optional<ProcessingMessage> build() {
    return Optional.of(new ProcessingMessage(this.message, this.helpUri, this.messageType,
        this.contextInfo, this.securityLevel));
  }

  @Override
  public @NotNull String toString() {
    return "ProcessingMessageOptionalBuilder{" +
        "message=" + this.message +
        ", contextInfo=" + this.contextInfo +
        ", helpUri=" + this.helpUri +
        ", messageType=" + this.messageType +
        ", securityLevel=" + this.securityLevel +
        "} " + super.toString();
  }
}
