/*
 * Copyright (c) 2024 Juraj Jurčo
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package church.i18n.processing.message;

import church.i18n.processing.builder.NullableBuilder;
import church.i18n.processing.security.policy.SecurityLevel;
import java.io.Serializable;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public interface ContextInfoBuilder extends NullableBuilder<ContextInfo> {

  /**
   * Add contextual information to builder.
   *
   * @param value     Contextual value.
   * @param valueType The type of the contextual value. E.g. number.
   * @return Instance of a builder.
   */
  @NotNull
  ContextInfoBuilder withContext(@Nullable Serializable value,
      @Nullable ValueType valueType);

  /**
   * Add contextual information to builder.
   *
   * @param value     Contextual value.
   * @param valueType The type of the contextual value. E.g. number.
   * @return Instance of a builder.
   */
  @NotNull
  ContextInfoBuilder withContext(@Nullable Serializable value,
      @Nullable String valueType);

  /**
   * Add contextual information to builder.
   *
   * @param value Contextual value.
   * @return Instance of a builder.
   */
  @NotNull
  ContextInfoBuilder withContext(@Nullable Serializable value);

  /**
   * Add contextual information to builder.
   *
   * @param context Contextual information.
   * @return Instance of a builder.
   */
  @NotNull
  ContextInfoBuilder withContext(@NotNull ContextValue context);

  /**
   * Add help information to builder.
   *
   * @param help Help information. E.g. regular expression matching correct string value.
   * @return Instance of a builder.
   */
  @NotNull
  ContextInfoBuilder withHelp(@Nullable ContextValue help);

  /**
   * Add help information to builder.
   *
   * @param help     Help information. E.g. regular expression matching correct string value.
   * @param helpType The type of the contextual value. E.g. string.
   * @return Instance of a builder.
   */
  @NotNull
  ContextInfoBuilder withHelp(@Nullable Serializable help,
      @Nullable ValueType helpType);

  /**
   * Add help information to builder.
   *
   * @param help     Help information. E.g. regular expression matching correct string value.
   * @param helpType The type of the contextual value. E.g. string.
   * @return Instance of a builder.
   */
  @NotNull
  ContextInfoBuilder withHelp(@Nullable Serializable help, @Nullable String helpType);

  /**
   * Add help information to builder.
   *
   * @param help Help information. E.g. regular expression matching correct string value.
   * @return Instance of a builder.
   */
  @NotNull
  ContextInfoBuilder withHelp(@Nullable Serializable help);

  /**
   * Add human-readable message to contextual information.
   *
   * @param message Message describing problem.
   * @return Instance of a builder.
   */
  @NotNull
  ContextInfoBuilder withMessage(@Nullable I18nMessage message);

  /**
   * Add human-readable message to contextual information.
   *
   * @param code   Code of a message from the resource bundle or free-text message.
   * @param params Parameters for {@link java.text.MessageFormat} if the message contains
   *               parameters.
   * @return Instance of a builder.
   */
  @NotNull
  ContextInfoBuilder withMessage(@Nullable String code,
      @Nullable Object... params);

  /**
   * Specify security level of information in the security context.
   *
   * @param securityLevel The security level of context information.
   * @return Instance of a builder.
   */
  @NotNull
  ContextInfoBuilder withSecurityLevel(@Nullable SecurityLevel securityLevel);
}
