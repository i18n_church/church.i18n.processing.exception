/*
 * Copyright (c) 2024 Juraj Jurčo
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package church.i18n.processing.message;

import church.i18n.processing.security.policy.SecurityLevel;
import java.io.Serializable;
import java.util.Objects;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class ContextInfoDefaultBuilder implements ContextInfoBuilder {

  private final @NotNull String name;
  private @Nullable ContextValue context;
  private @Nullable ContextValue help;
  private @Nullable I18nMessage message;
  private @Nullable SecurityLevel securityLevel;

  public ContextInfoDefaultBuilder(final @NotNull String name) {
    this.name = name;
  }

  @Override
  public @Nullable ContextInfo build() {
    return new ContextInfo(this.name, this.context, this.help, this.message, this.securityLevel);
  }

  @Override
  public @NotNull ContextInfoBuilder withContext(final @Nullable Serializable value,
      final @Nullable ValueType valueType) {
    this.context = new ContextValue(value, valueType);
    return this;
  }

  @Override
  public @NotNull ContextInfoBuilder withContext(final @Nullable Serializable value,
      final @Nullable String valueType) {
    this.context = new ContextValue(value, valueType);
    return this;
  }

  @Override
  public @NotNull ContextInfoBuilder withContext(final @Nullable Serializable value) {
    this.context = new ContextValue(value);
    return this;
  }

  @Override
  public @NotNull ContextInfoBuilder withContext(final @Nullable ContextValue context) {
    this.context = context;
    return this;
  }

  @Override
  public @NotNull ContextInfoBuilder withHelp(final @Nullable ContextValue help) {
    this.help = help;
    return this;
  }

  @Override
  public @NotNull ContextInfoBuilder withHelp(final @Nullable Serializable help,
      final @Nullable ValueType helpType) {
    this.help = new ContextValue(help, helpType);
    return this;
  }

  @Override
  public @NotNull ContextInfoBuilder withHelp(final @Nullable Serializable help,
      final @Nullable String helpType) {
    this.help = new ContextValue(help, helpType);
    return this;
  }

  @Override
  public @NotNull ContextInfoBuilder withHelp(final @Nullable Serializable help) {
    this.help = new ContextValue(help);
    return this;
  }

  @Override
  public @NotNull ContextInfoBuilder withMessage(final @Nullable I18nMessage message) {
    this.message = message;
    return this;
  }

  @Override
  public @NotNull ContextInfoBuilder withMessage(final @Nullable String code,
      final @Nullable Object... params) {
    if (code != null) {
      this.message = new I18nMessage(code, params);
    }
    return this;
  }

  @Override
  public @NotNull ContextInfoBuilder withSecurityLevel(final @Nullable SecurityLevel securityLevel) {
    this.securityLevel = securityLevel;
    return this;
  }

  @Override
  public int hashCode() {
    return Objects.hash(this.name, this.context, this.help, this.message, this.securityLevel);
  }

  @Override
  public boolean equals(final @Nullable Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ContextInfoDefaultBuilder that = (ContextInfoDefaultBuilder) o;
    return this.name.equals(that.name) &&
        Objects.equals(this.context, that.context) &&
        Objects.equals(this.help, that.help) &&
        Objects.equals(this.message, that.message) &&
        this.securityLevel == that.securityLevel;
  }

  @Override
  public @NotNull String toString() {
    return "ContextInfoDefaultBuilder{" +
        "name='" + this.name + '\'' +
        ", context=" + this.context +
        ", help=" + this.help +
        ", message=" + this.message +
        ", securityLevel=" + this.securityLevel +
        '}';
  }
}
