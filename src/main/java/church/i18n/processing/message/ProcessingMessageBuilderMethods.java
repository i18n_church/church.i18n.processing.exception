/*
 * Copyright (c) 2024 Juraj Jurčo
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package church.i18n.processing.message;

import church.i18n.processing.security.policy.SecurityLevel;
import java.net.URI;
import java.util.List;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public interface ProcessingMessageBuilderMethods<T extends ProcessingMessageBuilderMethods<T>> {

  /**
   * Add additional contextual information bounded to message. It appends this contextual
   * information to all previously added contextual information.
   *
   * @param contextInfo Contextual information you want to provide together with the message.
   * @return An instance of an {@link T} with appended contextual information.
   */
  @NotNull
  T addContextInfo(@Nullable List<ContextInfo> contextInfo);

  /**
   * Add contextual information to the message.
   *
   * @param contextInfo Additional contextual information appended to message.
   * @return An instance of an {@link T} with appended contextual information.
   */
  @NotNull
  T addContextInfo(@Nullable ContextInfo... contextInfo);

  /**
   * Help URI associated with this message that may help receiver to find more information about the
   * problem, better understand it and resolve it quickly. It can be reference to the documentation,
   * KB or other article describing problem.
   *
   * @param helpUri Resource identifier.
   * @return An instance of an {@link T} with appended contextual information.
   */
  @NotNull
  T withHelpUri(@Nullable URI helpUri);

  /**
   * Help URI associated with this message that may help receiver to find more information about the
   * problem, better understand it and resolve it quickly. It can be reference to the documentation,
   * KB or other article describing problem.
   *
   * @param helpUri Resource identifier.
   * @return An instance of an {@link T} with appended contextual information.
   */
  @NotNull
  T withHelpUri(@NotNull String helpUri);

  /**
   * Specify security level of information in message.
   *
   * @param securityLevel Security level of information in message.
   * @return An instance of an {@link T} with appended contextual information.
   */
  @NotNull
  T withSecurityLevel(@Nullable SecurityLevel securityLevel);

  /**
   * Type of message, such as severity or message status. For more information see {@link
   * MessageStatus} or {@link MessageSeverity}.
   *
   * @param messageType The type of this message.
   * @return An instance of an {@link T} with appended contextual information.
   */
  @NotNull
  T withMessageType(@NotNull MessageType messageType);
}
