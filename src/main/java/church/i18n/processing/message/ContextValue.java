/*
 * Copyright (c) 2024 Juraj Jurčo
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package church.i18n.processing.message;

import java.io.Serial;
import java.io.Serializable;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class ContextValue implements Serializable {

  @Serial
  private static final long serialVersionUID = -1724770248574818922L;
  private final @Nullable Serializable value;
  private final @Nullable String valueType;

  /**
   * Constructor of a value with its type.
   *
   * @param value     Value or data you want to assign.
   * @param valueType The type of the {@code value} argument you have provided. For example when
   *                  {@code value} is string you may specify what type of data it carries, e.g.
   *                  List, Enum, Regular expression or so. When it is an object you can closely
   *                  specify what format of object it is — e.g., JSON, XML.
   */
  public ContextValue(final @Nullable Serializable value, final @Nullable ValueType valueType) {
    this.value = value;
    this.valueType = (valueType == null) ? null : valueType.getValue();
  }

  /**
   * Constructor of a value with its type.
   *
   * @param value     Value or data you want to assign.
   * @param valueType The type of the {@code value} argument you have provided. For example when
   *                  {@code value} is string you may specify what type of data it carries, e.g.
   *                  List, Enum, Regular expression or so. When it is an object you can closely
   *                  specify what format of object it is — e.g., JSON, XML.
   */
  public ContextValue(final @Nullable Serializable value, final @Nullable String valueType) {
    this.value = value;
    this.valueType = valueType;
  }

  /**
   * Constructor of a value without specifying a type.
   *
   * @param value Value or data you want to assign.
   */
  public ContextValue(final @Nullable Serializable value) {
    this.value = value;
    this.valueType = null;
  }

  /**
   * Constructs a {@link ContextValue} from collection and assign the type of collection.
   *
   * @param collection Collections of values.
   * @param type       Type of a collection.
   * @param <C>        Type of objects in the collection.
   * @return A {@link ContextValue} with collection values and it's type.
   */
  public static @NotNull <C> ContextValue fromCollection(final @Nullable Collection<C> collection,
      final @NotNull ValueType type) {
    final String value = collection == null ? null : collection.toString();
    return new ContextValue(value, type.getValue());
  }

  /**
   * Constructs a {@link ContextValue} from all enumeration values.
   *
   * @param enumeration Enum to use.
   * @param <E>         Enum type.
   * @return A {@link ContextValue} with array of all enum values and as value type {@link
   *     ValueType#ENUM} is used.
   */
  public static @NotNull <E extends Enum<E>> ContextValue fromEnum(final @Nullable Class<E> enumeration) {
    final String value = enumeration == null
                         ? null
                         : Arrays.toString(enumeration.getEnumConstants());
    return new ContextValue(value, ValueType.ENUM);
  }

  /**
   * Constructs a {@link ContextValue} from listed enum values.
   *
   * @param enumeration List of enum values to use (Could be only subset of the enum)
   * @param <E>         Enum type.
   * @return A {@link ContextValue} with array of listed enum values and as value type {@link
   *     ValueType#ENUM} is used.
   */
  @SafeVarargs
  public static @NotNull <E extends Enum<E>> ContextValue fromEnum(final @Nullable Enum<E>... enumeration) {
    final String value = enumeration == null
                         ? null
                         : Arrays.toString(enumeration);
    return new ContextValue(value, ValueType.ENUM);
  }

  /**
   * Constructs a {@link ContextValue} from list of values.
   *
   * @param list List of values to use.
   * @param <L>  Type of objects in the list.
   * @return A {@link ContextValue} with array of list values and as value type {@link
   *     ValueType#LIST} is used.
   */
  public static @NotNull <L> ContextValue fromList(final @Nullable List<L> list) {
    final String value = list == null ? null : list.toString();
    return new ContextValue(value, ValueType.LIST);
  }

  /**
   * Constructs a {@link ContextValue} from map of key-value entries.
   *
   * @param map Map of key-pair entries to use.
   * @param <K> Type of map keys.
   * @param <V> Type of values in the map.
   * @return A {@link ContextValue} with all map key-value entries and as value type {@link
   *     ValueType#MAP} is used.
   */
  public static @NotNull <K, V> ContextValue fromMap(final @Nullable Map<K, V> map) {
    final String value = map == null ? null : map.toString();
    return new ContextValue(value, ValueType.MAP);
  }

  /**
   * Constructs a {@link ContextValue} from set of values.
   *
   * @param set Set of values to use.
   * @param <S> Type of objects in the set.
   * @return A {@link ContextValue} with array of set values and as value type {@link ValueType#SET}
   *     is used.
   */
  public static @NotNull <S> ContextValue fromSet(final @Nullable Set<S> set) {
    final String value = set == null ? null : set.toString();
    return new ContextValue(value, ValueType.SET);
  }

  /**
   * Get assigned value.
   *
   * @return Assigned value.
   */
  public @Nullable Object getValue() {
    return this.value;
  }

  /**
   * Get assigned value type.
   *
   * @return Assigned value type.
   */
  public @Nullable String getValueType() {
    return this.valueType;
  }

  @Override
  public int hashCode() {
    return Objects.hash(this.value, this.valueType);
  }

  @Override
  public boolean equals(final Object o) {
    if (this == o) {
      return true;
    }
    if (!(o instanceof final ContextValue that)) {
      return false;
    }
    return Objects.equals(this.value, that.value) &&
        Objects.equals(this.valueType, that.valueType);
  }

  @Override
  public @NotNull String toString() {
    return "ContextValue{"
        + "value='" + this.value + '\''
        + ", valueType='" + this.valueType + '\''
        + '}';
  }
}
