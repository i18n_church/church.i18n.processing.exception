/*
 * Copyright (c) 2024 Juraj Jurčo
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package church.i18n.processing.message;

import church.i18n.processing.builder.GenericBuilder;
import church.i18n.processing.storage.MessageStorage;
import org.jetbrains.annotations.NotNull;

/**
 * Interface adding extra functionality specific to handling Processing Messages.
 *
 * @param <T> Type of building objects.
 */
public interface ProcessingMessageBuilder<T> extends
    ProcessingMessageBuilderMethods<ProcessingMessageBuilder<T>>,
    GenericBuilder<T> {

  /**
   * It's terminal action of building when the message is not directly returned to the consumer,
   * rather placed into a message storage directly.
   *
   * @param storage Storage of messages for current context.
   */
  void addToMessageStorage(@NotNull MessageStorage storage);

}
