/*
 * Copyright (c) 2024 Juraj Jurčo
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package church.i18n.processing.message;

import church.i18n.processing.security.policy.SecurityLevel;
import java.io.Serial;
import java.io.Serializable;
import java.net.URI;
import java.util.List;
import java.util.Objects;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

/**
 * A generic definition of a processing message that carries information processing. A typical usage
 * is during a processing error; however it's not bounded only to this type.
 */
public class ProcessingMessage implements Serializable {

  @Serial
  private static final long serialVersionUID = 7475226882356640829L;

  private final @NotNull I18nMessage message;
  private final @Nullable URI helpUri;
  private final @NotNull MessageType messageType;
  private final @NotNull List<ContextInfo> contextInfo;
  private final @Nullable SecurityLevel securityLevel;

  /**
   * Constructor of a processing message
   *
   * @param code   The code reference in the localization property file.
   * @param params Parameters that needs to be provided to properly format the message with all.
   *               parameters.
   */
  public ProcessingMessage(final @NotNull String code, final @Nullable Object... params) {
    this(new I18nMessage(code, params));
  }

  /**
   * The constructor with the message. The message is required. As a {@link MessageType} the value
   * of {@link DefaultMessageType#DEFAULT} is used.
   *
   * @param message Raw response message that is not formatted yet.
   */
  public ProcessingMessage(final @NotNull I18nMessage message) {
    this(message, null, DefaultMessageType.DEFAULT, List.of(), null);
  }

  public ProcessingMessage(
      final @NotNull I18nMessage message,
      final @Nullable URI helpUri,
      final @NotNull MessageType messageType,
      final @NotNull List<ContextInfo> contextInfo,
      final @Nullable SecurityLevel securityLevel) {
    this.message = message;
    this.helpUri = helpUri;
    this.messageType = messageType;
    this.contextInfo = contextInfo;
    this.securityLevel = securityLevel;
  }

  /**
   * Create a builder of {@link ProcessingMessage} with specified message code and parameters.
   *
   * @param code   The code reference in the localization property file.
   * @param params Parameters that needs to be provided to properly format the message with all.
   * @return Builder of the {@link ProcessingMessage} object.
   */
  public static @NotNull ProcessingMessageBuilder<ProcessingMessage> withMessage(final @NotNull String code,
      final @Nullable Object... params) {
    return new ProcessingMessageDefaultBuilder(code, params);
  }

  /**
   * Create a builder of {@link ProcessingMessage} with specified {@link I18nMessage} message.
   *
   * @param message Raw response message that is not formatted yet.
   * @return Builder of the {@link ProcessingMessage} object.
   */
  public static @NotNull ProcessingMessageBuilder<ProcessingMessage> withMessage(
      final @NotNull I18nMessage message) {
    return new ProcessingMessageDefaultBuilder(message);
  }


  /**
   * Copy constructor for the {@link ProcessingMessage} object.
   *
   * @param message Response message you would like to make a copy of.
   * @return Builder of the {@link ProcessingMessage} object.
   */
  public static @NotNull ProcessingMessageBuilder<ProcessingMessage> withMessage(
      final @NotNull ProcessingMessage message) {
    return new ProcessingMessageDefaultBuilder(message);
  }

  public @NotNull List<ContextInfo> getContextInfo() {
    return this.contextInfo;
  }

  public @Nullable URI getHelpUri() {
    return this.helpUri;
  }

  public @NotNull I18nMessage getMessage() {
    return this.message;
  }

  public @NotNull MessageType getMessageType() {
    return this.messageType;
  }

  public @Nullable SecurityLevel getSecurityLevel() {
    return this.securityLevel;
  }

  @Override
  public int hashCode() {
    return Objects.hash(this.message, this.helpUri, this.messageType, this.contextInfo,
        this.securityLevel);
  }

  @Override
  public boolean equals(final Object o) {
    if (this == o) {
      return true;
    }
    if (!(o instanceof final ProcessingMessage that)) {
      return false;
    }
    return this.message.equals(that.message) &&
        Objects.equals(this.helpUri, that.helpUri) &&
        this.messageType.equals(that.messageType) &&
        this.contextInfo.equals(that.contextInfo) &&
        this.securityLevel == that.securityLevel;
  }

  @Override
  public @NotNull String toString() {
    return "ProcessingMessage{"
        + "message=" + this.message
        + ", helpUri=" + this.helpUri
        + ", messageType=" + this.messageType
        + ", contextInfo=" + this.contextInfo
        + ", securityLevel=" + this.securityLevel
        + '}';
  }
}
