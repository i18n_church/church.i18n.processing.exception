/*
 * Copyright (c) 2024 Juraj Jurčo
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package church.i18n.processing.message;

import church.i18n.processing.security.policy.SecurityLevel;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Generic builder content for Processing message.
 *
 * @param <T> Type of implemented builder for a more specific implementation.
 */
@SuppressWarnings("unchecked")
public class ProcessingMessageAbstractBuilder<T extends ProcessingMessageBuilderMethods<T>>
    implements ProcessingMessageBuilderMethods<T> {

  private static final Logger log = LoggerFactory.getLogger(ProcessingMessageAbstractBuilder.class);

  protected final @NotNull I18nMessage message;
  protected final @NotNull List<ContextInfo> contextInfo = new ArrayList<>();
  protected @Nullable URI helpUri;
  protected @NotNull MessageType messageType = DefaultMessageType.DEFAULT;
  protected @Nullable SecurityLevel securityLevel;


  public ProcessingMessageAbstractBuilder(final @NotNull String code,
      final @Nullable Object... params) {
    this.message = new I18nMessage(code, params);
  }

  public ProcessingMessageAbstractBuilder(final @NotNull I18nMessage message) {
    this.message = message;
  }

  public ProcessingMessageAbstractBuilder(final @NotNull ProcessingMessage processingMessage) {
    this.message = processingMessage.getMessage();
    this.helpUri = processingMessage.getHelpUri();
    this.messageType = processingMessage.getMessageType();
    this.securityLevel = processingMessage.getSecurityLevel();
    this.contextInfo.addAll(processingMessage.getContextInfo());
  }

  @Override
  public @NotNull T addContextInfo(final @Nullable List<ContextInfo> contextInfo) {
    if (contextInfo != null) {
      this.contextInfo.addAll(contextInfo);
    }
    return (T) this;
  }

  @Override
  public @NotNull T addContextInfo(final @Nullable ContextInfo... contextInfo) {
    if (contextInfo != null) {
      Arrays.stream(contextInfo)
          .filter(Objects::nonNull)
          .forEach(this.contextInfo::add);
    }
    return (T) this;
  }

  @Override
  public @NotNull T withHelpUri(final @Nullable URI helpUri) {
    this.helpUri = helpUri;
    return (T) this;
  }

  @Override
  public @NotNull T withHelpUri(final @NotNull String helpUri) {
    try {
      this.helpUri = new URI(helpUri);
    } catch (URISyntaxException e) {
      log.error("String '{}' could not be parsed as a URI reference.", helpUri, e);
    }
    return (T) this;
  }

  @Override
  public @NotNull T withSecurityLevel(final @Nullable SecurityLevel securityLevel) {
    this.securityLevel = securityLevel;
    return (T) this;
  }

  @Override
  public @NotNull T withMessageType(final @NotNull MessageType messageType) {
    this.messageType = messageType;
    return (T) this;
  }

  @Override
  public int hashCode() {
    return Objects.hash(this.message, this.contextInfo, this.helpUri, this.messageType,
        this.securityLevel);
  }

  @Override
  public boolean equals(final Object o) {
    if (this == o) {
      return true;
    }
    if (!(o instanceof final ProcessingMessageAbstractBuilder<?> that)) {
      return false;
    }
    return this.message.equals(that.message) && this.contextInfo.equals(that.contextInfo) && Objects
        .equals(this.helpUri, that.helpUri) && Objects.equals(this.messageType, that.messageType)
        && this.securityLevel == that.securityLevel;
  }

  @Override
  public @NotNull String toString() {
    return "ProcessingMessageAbstractBuilder{" +
        "message=" + this.message +
        ", helpUri=" + this.helpUri +
        ", messageType=" + this.messageType +
        ", contextInfo=" + this.contextInfo +
        ", securityLevel=" + this.securityLevel +
        '}';
  }
}
