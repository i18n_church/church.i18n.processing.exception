/*
 * Copyright (c) 2024 Juraj Jurčo
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package church.i18n.processing.message;

import java.io.Serial;
import java.util.Arrays;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

/**
 * Typical severities and statuses of message.
 */
public enum MessageSeverity implements MessageType {
  CRITICAL,
  URGENT,
  HIGH,
  MODERATE,
  NORMAL,
  MINOR,
  LOW;

  @Serial
  private static final long serialVersionUID = 1368843097919838190L;

  /**
   * Match a string value with enum values and return case-insensitive matching enum value. If no value matches, return
   * a {@code defaultValue}.
   *
   * @param string       A string value to lookup.
   * @param defaultValue A default value to return when nothing matches.
   * @return A case-insensitive enum value matching string; {@code defaultValue} otherwise.
   */
  public static @NotNull MessageSeverity getOrDefault(final @Nullable String string,
      final @NotNull MessageSeverity defaultValue) {
    return Arrays.stream(values())
        .filter(value -> value.name().equalsIgnoreCase(string))
        .findFirst()
        .orElse(defaultValue);
  }

  @Override
  public @NotNull String getType() {
    return name();
  }
}
