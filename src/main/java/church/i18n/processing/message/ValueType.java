/*
 * Copyright (c) 2025 Juraj Jurčo
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package church.i18n.processing.message;

import java.io.Serial;

/**
 * There are different types of formats that can be provided. This enum lists the most common types of such format. E.g.
 * you may need to express user, that e-mail value should be of format: ^\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}$ so in this case
 * you specify 'REGEXP. When provided format is: [STRING, ENUM, LIST, REGEXP], you specify ENUM since it refers to the
 * list of possible enumerated string values.
 */
public enum ValueType {
  BOOLEAN("boolean"),
  INTEGER("integer"),
  DECIMAL("decimal"),
  NUMBER("number"),
  STRING("string"),
  DATETIME("datetime"),
  REGEXP("regexp"),
  SEMVER("semver"),
  UUID("uuid"),

  ENUM("enum"),
  LIST("list"),
  SET("set"),
  MAP("map"),

  JSON("json"),
  XML("xml"),

  URL("url"),
  URI("uri"),

  OBJECT("object"),
  TYPE("type"),

  /**
   * A special type telling that consumer should follow serialization hints of response. Use this type in the case you
   * pass an object as a value and you expect that correct type will be set at the time of serialization. This might be
   * helpful when using multiple serialization types as response.
   */
  AUTO_MIME_TYPE("auto-mime-type");

  @Serial
  private static final long serialVersionUID = -8847715468140552333L;

  private final String value;

  ValueType(final String value) {
    this.value = value;
  }

  public String getValue() {
    return value;
  }
}
