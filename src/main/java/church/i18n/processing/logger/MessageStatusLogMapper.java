/*
 * Copyright (c) 2024 Juraj Jurčo
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package church.i18n.processing.logger;

import church.i18n.processing.config.ProcessingExceptionConfig;
import church.i18n.processing.exception.ProcessingException;
import church.i18n.processing.message.MessageStatus;
import church.i18n.processing.message.MessageType;
import church.i18n.processing.security.sanitizer.SecurityInfoSanitizer;
import java.util.Optional;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MessageStatusLogMapper implements LogMapper {

  private static final @NotNull Logger log = LoggerFactory.getLogger(MessageStatusLogMapper.class);
  private final @NotNull SecurityInfoSanitizer sanitizer;
  private final @NotNull ProcessingExceptionConfig config;

  public MessageStatusLogMapper(final @NotNull SecurityInfoSanitizer sanitizer,
      final @NotNull ProcessingExceptionConfig config) {
    this.sanitizer = sanitizer;
    this.config = config;
  }

  @Override
  public void log(final @NotNull ProcessingException exception) {
    final Optional<ProcessingException> sanitizedException = this.sanitizer.sanitize(exception,
        this.config);
    if (sanitizedException.isPresent()) {
      ProcessingException secException = sanitizedException.get();

      String loggedMessage = secException.getProcessingMessage().getMessage().getCode();
      final MessageType messageType = exception.getProcessingMessage().getMessageType();
      if (messageType instanceof MessageStatus) {
        switch ((MessageStatus) messageType) {
          case INFO:
            log.info(loggedMessage, secException);
            return;
          case WARNING:
            log.warn(loggedMessage, secException);
            return;
          case ERROR:
            log.error(loggedMessage, secException);
            return;
          default:
            log.error("The message status level '{}' is not implemented!", messageType);
            log.error(loggedMessage, secException);
            return;
        }
      }
      log.error("This log mapper is unable to handle '{}' message type. Please consider to fix it.",
          messageType);
      log.error(loggedMessage, secException);
    }
  }

  @Override
  public @NotNull String toString() {
    return "MessageStatusLogMapper{" +
        "sanitizer=" + this.sanitizer +
        ", config=" + this.config +
        '}';
  }
}
