/*
 * Copyright (c) 2024 Juraj Jurčo
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package church.i18n.processing.logger;

import church.i18n.processing.config.ProcessingExceptionConfig;
import church.i18n.processing.exception.ProcessingException;
import church.i18n.processing.message.MessageSeverity;
import church.i18n.processing.message.MessageStatus;
import church.i18n.processing.message.MessageType;
import church.i18n.processing.security.sanitizer.SecurityInfoSanitizer;
import java.util.Map;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MessageTypeLogMapper implements LogMapper {

  private static final Logger log = LoggerFactory.getLogger(MessageTypeLogMapper.class);
  private final @NotNull Map<Class<? extends MessageType>, LogMapper> mappers;

  public MessageTypeLogMapper(final @NotNull SecurityInfoSanitizer sanitizer,
      final @NotNull ProcessingExceptionConfig config) {
    this(Map.of(
        MessageStatus.class, new MessageStatusLogMapper(sanitizer, config),
        MessageSeverity.class, new MessageSeverityLogMapper(sanitizer, config)
    ));
  }

  public MessageTypeLogMapper(final @NotNull Map<Class<? extends MessageType>, LogMapper> mappers) {
    this.mappers = mappers;
  }

  @Override
  public void log(final @NotNull ProcessingException exception) {
    final MessageType messageType = exception.getProcessingMessage().getMessageType();
    final LogMapper logMapper = this.mappers.get(messageType.getClass());
    if (logMapper != null) {
      logMapper.log(exception);
      return;
    }

    log.error("There is no log mapper registered for a class '{}'. Please consider to fix it.",
        messageType.getClass());
    if (log.isErrorEnabled()) {
      log.error(exception.toString(), exception);
    }
  }

  @Override
  public @NotNull String toString() {
    return "MessageTypeLogMapper{" +
        ", mappers=" + this.mappers +
        '}';
  }
}
