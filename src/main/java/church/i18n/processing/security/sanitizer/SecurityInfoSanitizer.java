/*
 * Copyright (c) 2024 Juraj Jurčo
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package church.i18n.processing.security.sanitizer;

import church.i18n.processing.config.ProcessingExceptionConfig;
import church.i18n.processing.exception.ProcessingException;
import church.i18n.processing.message.ContextInfo;
import church.i18n.processing.message.ProcessingMessage;
import church.i18n.processing.security.policy.SecurityPolicy;
import java.util.List;
import java.util.Optional;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public interface SecurityInfoSanitizer {

  /**
   * Deeply sanitize exception's message and it's context-info values.
   *
   * @param exception An exception to be sanitized.
   * @param config    A config used for sanitization.
   * @return Deep {@link ProcessingException} sanitization according to a configuration. Any of
   *     sub-objects that are subject of sanitization does not meet config criteria are dropped. In
   *     the case {@link ProcessingMessage} is dropped, {@link Optional#empty()} is returned.
   *     Otherwise a new sanitized exception is constructed.
   */
  default @NotNull Optional<ProcessingException> sanitize(
      final @NotNull ProcessingException exception,
      final @NotNull ProcessingExceptionConfig config) {
    final Optional<ProcessingMessage> sanitizedMessage = sanitize(exception.getProcessingMessage(),
        config);
    return sanitizedMessage
        .map(message -> {
              ProcessingException processingException = new ProcessingException(message,
                  exception.getCause())
                  .withLogLevel(exception.getLogLevel())
                  .withStatus(exception.getStatus());
              processingException.setStackTrace(exception.getStackTrace());
              return processingException;
            }
        );
  }

  /**
   * Deeply sanitize message and it's context-info values.
   *
   * @param message A message to be sanitized.
   * @param config  A config used for sanitization.
   * @return Deep {@link ProcessingMessage} sanitization according to a configuration. Any of
   *     sub-objects that are subject of sanitization does not meet config criteria are dropped. In
   *     the case {@link ProcessingMessage} alone is dropped, {@link Optional#empty()} is returned.
   *     Otherwise a new sanitized {@link ProcessingMessage} is constructed.
   */
  default @NotNull Optional<ProcessingMessage> sanitize(final @Nullable ProcessingMessage message,
      final @NotNull ProcessingExceptionConfig config) {
    if (message == null || !canPublish(message.getSecurityLevel(), config)) {
      return Optional.empty();
    }
    final List<ContextInfo> sanitizedContext = message.getContextInfo()
        .stream()
        .map(contextInfo -> sanitize(contextInfo, config))
        .flatMap(Optional::stream)
        .toList();
    final ProcessingMessage resultMessage = ProcessingMessage
        .withMessage(message.getMessage())
        .withSecurityLevel(message.getSecurityLevel())
        .withHelpUri(message.getHelpUri())
        .withMessageType(message.getMessageType())
        .addContextInfo(sanitizedContext)
        .build();
    return Optional.of(resultMessage);
  }

  /**
   * Sanitize context-info.
   *
   * @param contextInfo A context-info to be sanitized.
   * @param config      A config used for sanitization.
   * @return {@link ContextInfo} sanitization according to a configuration. In the case {@link
   *     ContextInfo} is sanitized, {@link Optional#empty()} is returned. Otherwise the same object
   *     {@link ContextInfo} is returned in {@link Optional#of(Object)}.
   */
  default @NotNull Optional<ContextInfo> sanitize(final @Nullable ContextInfo contextInfo,
      final @NotNull ProcessingExceptionConfig config) {
    if (contextInfo != null && canPublish(contextInfo.getSecurityLevel(), config)) {
      return Optional.of(contextInfo);
    }
    return Optional.empty();
  }

  /**
   * Method checks whether information with {@code infoSecurityPolicy} could be published or not
   * within given configuration.
   *
   * @param infoSecurityPolicy Information's security policy.
   * @param config             A configuration used for evaluation of security policy information.
   * @return {@code true} in the case information with given security policy could be published;
   *     {@code false} otherwise.
   */
  boolean canPublish(@Nullable SecurityPolicy infoSecurityPolicy,
      @NotNull ProcessingExceptionConfig config);

}
