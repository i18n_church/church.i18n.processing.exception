/*
 * Copyright (c) 2024 Juraj Jurčo
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package church.i18n.processing.security.policy;

import java.util.Arrays;
import java.util.Map;
import java.util.Objects;
import java.util.function.Function;
import java.util.stream.Collectors;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

/**
 * Different security levels of information that often occurs in exception handling. In short, it lists following types of security information types:
 * <ul>
 * <li>{@link SecurityLevel#PUBLIC}</li>
 * <li>{@link SecurityLevel#SYSTEM_EXTERNAL}</li>
 * <li>{@link SecurityLevel#SYSTEM_INTERNAL}</li>
 * <li>{@link SecurityLevel#THIRD_PARTY}</li>
 * <li>{@link SecurityLevel#PERSONAL_INFORMATION}</li>
 * <li>{@link SecurityLevel#CONFIDENTIAL}</li>
 * <li>{@link SecurityLevel#RESTRICTED}</li>
 * </ul>
 * <p>
 * Levels are ordered from weakest to strongest.
 */
public enum SecurityLevel implements SecurityPolicy {

  /**
   * Public information is general message that does not disclose any information about system. It is information that is already publicly known or information that is known in user's context.
   *
   * <p>Example of public information is age restriction for registration, expected format of some
   * input values.
   *
   * <p>Disclosure of such information usually should not put a system into a risk.
   */
  PUBLIC,

  /**
   * System external information is exposing information of the system that are potentially know. User may get know this information with none or some effort.
   *
   * <p>Typically, external system information could be HTTP methods of particular endpoint,
   * required endpoint parameters, validation values ranges or their format.
   *
   * <p>Disclosure of such information may give attacker insight into external system facade and/or
   * its constraints.
   */
  SYSTEM_EXTERNAL,
  /**
   * System internal is exposing internal state or behavior of the system. This information may impose system into a risk.
   *
   * <p>Typically, internal information could be a path to a file, full class name, non-public
   * system settings, used libraries with their versions or similar information.
   *
   * <p>Disclosure of such information may give attacker deeper insight into the system and may
   * help to precisely target its vulnerabilities.
   */
  SYSTEM_INTERNAL,
  /**
   * Third party information is coming from third party libraries and there is a risk of exposing sensitive or internal information. This level of sensitivity is similar to {@link SecurityLevel#SYSTEM_INTERNAL} with the difference that value is taken directly
   * from the third party exception or its message. This information should help easy identification of the cause of problem.
   *
   * <p>Typically, third party information is vendor error code (e.g. SQLException#getSQLState ), or content of exception message.
   *
   * <p>Disclosure may potentially expose any kind of sensitive information. Full third party
   * exception messages could be particularly dangerous as you do not have a control over what the message contains. Future versions of library may change message, and you may not realize sensitivity of exposed content.
   */
  THIRD_PARTY,
  /**
   * Personal identifiable information (PII) is data that can be used to identify an individual directly and indirectly. Examples of such information could be (not limited to): name, surname, home address, e-mail address, national ID, phone number, date of
   * birth, bank account number, photo, geolocation, biometric information, behavioral data and similarly. Those information could be subject of law in a particular country, please find it out for yourself what information belongs to this category.
   *
   * <p>Disclosure or careless handling with such information may negatively affect business and
   * result in financial or legal impact to the business.
   */
  PERSONAL_INFORMATION,
  /**
   * Confidential information is sensitive and should be used only on limited and need-to-know basis. This information should not be exposed and accessed only through system logs. This information should help to investigate the cause of a problem. Access
   * should be audited.
   *
   * <p>Typically, sensitive information may include anonymized user data, pricing information,
   * reports, etc.
   *
   * <p>Disclosure of such information may negatively affect business and ultimately your brand.
   */
  CONFIDENTIAL,
  /**
   * Restricted information is highly sensitive. This level should be used only on a very limited and only need-to-know basis. The level should not ever be exposed, only logged. One may enable logging only in the case of problem investigation and only if there
   * is no other way how to retrieve information of what happens in the system. Access should be audited.
   *
   * <p>Typically, restricted information contains potentially identifiable information, user
   * information or other business secrets.
   *
   * <p>Disclosure of such information may result in a significant financial or legal impact to the
   * business.
   */
  RESTRICTED;


  /**
   * Parse values and return SecurityLevel enum items.
   *
   * @param values values to parse.
   * @return Array of parsed items in the case
   */
  public static @NotNull SecurityLevel[] parse(final @Nullable String... values) {
    if (values == null) {
      return new SecurityLevel[0];
    }
    final Map<String, SecurityLevel> levelMap = Arrays.stream(values())
        .collect(Collectors.toUnmodifiableMap(l -> l.name().toUpperCase(), Function.identity()));
    return Arrays.stream(values)
        .filter(Objects::nonNull)
        .map(String::toUpperCase)
        .map(levelMap::get)
        .filter(Objects::nonNull)
        .toArray(SecurityLevel[]::new);
  }
}
