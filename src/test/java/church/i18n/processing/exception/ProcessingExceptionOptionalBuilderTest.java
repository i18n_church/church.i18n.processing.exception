/*
 * Copyright (c) 2024 Juraj Jurčo
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package church.i18n.processing.exception;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import church.i18n.processing.logger.LogLevel;
import church.i18n.processing.message.I18nMessage;
import church.i18n.processing.message.ProcessingMessage;
import church.i18n.processing.status.ClientError;
import java.util.Optional;
import org.junit.jupiter.api.Test;

class ProcessingExceptionOptionalBuilderTest {

  @Test
  void build() {
    final Optional<ProcessingException> build = new ProcessingExceptionOptionalBuilder("err-code")
        .build();
    assertTrue(build.isPresent());
    final ProcessingException exception = build.get();
    assertEquals("err-code", exception.getProcessingMessage().getMessage().getCode());
  }

  @Test
  void throwException() {
    assertThrows(ProcessingException.class,
        () -> new ProcessingExceptionOptionalBuilder(new I18nMessage("err-code")).throwException());
  }

  @Test
  void withLogLevel() {
    final Optional<ProcessingException> build =
        new ProcessingExceptionOptionalBuilder(new ProcessingMessage("err-code"))
            .withLogLevel(LogLevel.TRACE)
            .build();
    assertTrue(build.isPresent());
    assertEquals(LogLevel.TRACE, build.get().getLogLevel());
  }

  @Test
  void withStatus() {
    final Optional<ProcessingException> build =
        new ProcessingExceptionOptionalBuilder(new ProcessingMessage("err-code"), new Throwable())
            .withStatus(ClientError.GONE)
            .build();
    assertTrue(build.isPresent());
    assertEquals(ClientError.GONE, build.get().getStatus());
  }
}
