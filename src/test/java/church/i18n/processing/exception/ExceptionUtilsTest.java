/*
 * Copyright (c) 2024 Juraj Jurčo
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package church.i18n.processing.exception;

import static church.i18n.processing.message.DefaultMessageType.DEFAULT;
import static church.i18n.processing.message.MessageStatus.INFO;
import static church.i18n.processing.message.MessageStatus.WARNING;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertInstanceOf;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import church.i18n.processing.message.ContextInfo;
import church.i18n.processing.status.ClientError;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Modifier;
import java.util.Optional;
import java.util.stream.Stream;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

class ExceptionUtilsTest {

  @Test
  void testCause() {
    ProcessingException originalWithThrowable = new ProcessingException("err-r-2", "param1",
        "param2", new IllegalArgumentException());
    ProcessingException mappedException = ExceptionUtils
        .wrap(originalWithThrowable, "err-r-3", "param3");
    assertEquals(originalWithThrowable.getCause(), mappedException.getCause());
  }

  @Test
  void testExceptionContent() {
    ProcessingException fullException = new ProcessingException("err-r-2", "param1", "param2",
        new IllegalArgumentException())
        .withStatus(ClientError.BAD_REQUEST)
        .withMessageType(WARNING)
        .withHelpUri("https://error.i18n.church")
        .addContextInfo(
            ContextInfo
                .of("name")
                .withContext("type", "value")
                .withHelp("format", "formatType")
                .withMessage("message").build()
        );
    ProcessingException mappedException = ExceptionUtils
        .wrap(fullException, "err-r-3", "param3");

    assertTrue(mappedException.getProcessingMessage().getContextInfo().isEmpty());
    assertNull(mappedException.getProcessingMessage().getHelpUri());
    assertEquals("err-r-3", mappedException.getProcessingMessage().getMessage().getCode());
    assertEquals(0, mappedException.getStatus().getStatusId(), 0);
    assertEquals(DEFAULT, mappedException.getProcessingMessage().getMessageType());
    assertEquals(1, mappedException.getProcessingMessage().getMessage().getMessageParams().length);
    assertEquals("param3",
        mappedException.getProcessingMessage().getMessage().getMessageParams()[0]);
  }

  @Test
  void testRemapWrappedCause() {
    ProcessingException originalWithThrowable = new ProcessingException("err-r-2", "param1",
        "param2", new IllegalArgumentException());
    ProcessingException expected = new ProcessingException("err-g-1", originalWithThrowable);

    assertEquals(expected, ExceptionUtils.rewrapWrapped(originalWithThrowable, "err-g-1"));
    assertEquals(expected, ExceptionUtils.rewrapWrapped(originalWithThrowable, "err-g-1",
        (Object[]) null));
    assertEquals(originalWithThrowable, expected.getCause());
  }

  @ParameterizedTest
  @MethodSource("noCauseExceptions")
  void testRemapWrappedException_noRootCause(final Throwable exception) {
    ProcessingException expected = new ProcessingException("err-g-1", exception);
    assertEquals(expected, ExceptionUtils.rewrapWrapped(exception, "err-g-1"));
  }

  @Test
  void testRemapWrappedException_nullParams() {
    ProcessingException expected = new ProcessingException("err-g-1", (Object[]) null);
    assertEquals(expected,
        ExceptionUtils.rewrapWrapped(new Exception(), "err-g-1", (Object[]) null));
  }

  @Test
  void testRemapWrappedInnerCause() {
    Exception innerException = new IllegalArgumentException(
        new RuntimeException(
            new ProcessingException("err-r-2", "param1", "param2", new IllegalArgumentException())
                .withStatus(ClientError.BAD_REQUEST)
                .withMessageType(INFO)
                .withHelpUri("https://error.i18n.church")
        ));
    ProcessingException expected = new ProcessingException("err-g-1", innerException);

    assertEquals(expected, ExceptionUtils.rewrapWrapped(innerException, "err-g-1"));
    assertEquals(innerException, expected.getCause());
  }

  @Test
  void testStackTrace() {
    ProcessingException original = new ProcessingException("err-r-2", "param1", "param2");
    ProcessingException mappedException = ExceptionUtils.wrap(original, "err-r-3", "param3");
    assertEquals(original.getCause(), mappedException.getCause());
  }

  @Test
  void testUtilityClassModifiersAndConstructor() throws NoSuchMethodException {
    assertTrue(Modifier.isFinal(ExceptionUtils.class.getModifiers()));
    Constructor<ExceptionUtils> constructor = ExceptionUtils.class.getDeclaredConstructor();
    assertTrue(Modifier.isPrivate(constructor.getModifiers()));
    constructor.setAccessible(true);
    final InvocationTargetException exception = assertThrows(InvocationTargetException.class,
        constructor::newInstance);
    assertInstanceOf(IllegalStateException.class, exception.getTargetException());
  }

  @ParameterizedTest
  @MethodSource("noCauseExceptions")
  void testWrappedException_noRootCause(final Throwable exception) {
    assertEquals(Optional.empty(), ExceptionUtils.getWrappedException(exception));
  }

  @Test
  void testWrappedException_null() {
    assertEquals(Optional.empty(), ExceptionUtils.getWrappedException(null));
  }

  private static Stream<Arguments> noCauseExceptions() {
    Throwable recursive = new Throwable();
    Throwable recursive_loop = new Throwable(recursive);
    recursive.initCause(recursive_loop);
    return Stream.of(
        Arguments.of(new Throwable()),
        Arguments.of(new Throwable(new IllegalArgumentException())),
        Arguments.of(recursive)
    );
  }
}
