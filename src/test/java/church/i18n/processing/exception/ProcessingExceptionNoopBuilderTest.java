/*
 * Copyright (c) 2024 Juraj Jurčo
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package church.i18n.processing.exception;

import static church.i18n.processing.message.MessageStatus.ERROR;
import static org.junit.jupiter.api.Assertions.assertTrue;

import church.i18n.processing.logger.LogLevel;
import church.i18n.processing.message.ContextInfo;
import church.i18n.processing.security.policy.SecurityLevel;
import church.i18n.processing.status.ClientError;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import org.junit.jupiter.api.Test;

class ProcessingExceptionNoopBuilderTest {

  @Test
  void build() throws URISyntaxException {
    final Optional<ProcessingException> build =
        ProcessingExceptionNoopBuilder.NO_OP_PROCESSING_EXCEPTION_BUILDER
            .addContextInfo(null, ContextInfo.of("asd01").build())
            .addContextInfo(List.of(ContextInfo.of("asd02").build()))
            .withHelpUri(new URI("https://prayers.church"))
            .withHelpUri("https://prayers.church")
            .withLogLevel(LogLevel.TRACE)
            .withSecurityLevel(SecurityLevel.CONFIDENTIAL)
            .withMessageType(ERROR)
            .withStatus(ClientError.BAD_REQUEST)
            .build();
    assertTrue(build.isEmpty());
  }

  @Test
  void throwException_nothingIsThrown() {
    ProcessingExceptionNoopBuilder.NO_OP_PROCESSING_EXCEPTION_BUILDER
        .throwException();
  }
}
