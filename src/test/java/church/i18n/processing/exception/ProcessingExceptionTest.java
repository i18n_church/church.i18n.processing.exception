/*
 * Copyright (c) 2024 Juraj Jurčo
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package church.i18n.processing.exception;

import static church.i18n.processing.message.MessageStatus.INFO;
import static church.i18n.processing.message.MessageStatus.WARNING;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import church.i18n.processing.logger.LogLevel;
import church.i18n.processing.message.ContextInfo;
import church.i18n.processing.message.ContextValue;
import church.i18n.processing.message.I18nMessage;
import church.i18n.processing.message.ProcessingMessage;
import church.i18n.processing.message.ValueType;
import church.i18n.processing.security.policy.SecurityLevel;
import church.i18n.processing.status.ClientError;
import church.i18n.processing.status.DefaultStatus;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.stream.Stream;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

public class ProcessingExceptionTest {

  private static final String NAME = "name";
  private static final String TYPE = "type";
  private static final String VALUE = "value";
  private static final String FORMAT = "format";
  private static final String FORMAT_TYPE = "formatType";
  private static final ContextValue CONTEXT_VALUE = new ContextValue(FORMAT, FORMAT_TYPE);
  private static final I18nMessage CONTEXT_MESSAGE = new I18nMessage("{0} {1} {0,number,currency}",
      123345, "as currency:");
  //copy builder
  private static final ContextInfo COPY_BUILDER_CONTEXT_INFO = new ContextInfo(
      NAME, new ContextValue(TYPE, VALUE), CONTEXT_VALUE, CONTEXT_MESSAGE, SecurityLevel.PUBLIC
  );

  public static Stream<Arguments> exceptionSources() throws URISyntaxException {
    return Stream.of(
        Arguments.of(new ProcessingException("msg-A1")),
        Arguments.of(new ProcessingException("msg-A2", 1234567.987)),
        Arguments.of(new ProcessingException("msg-A2", 1234567.987, new Throwable("SampleEx"))),
        Arguments.of(new ProcessingException("msg-A2", 87.097).addContextInfo((ContextInfo) null)),
        Arguments.of(new ProcessingException("msg-A2", 1, new Exception("Sample exception"))),
        Arguments.of(new ProcessingException("msg-A2", 123.123, new Exception("Source cause"))),
        Arguments.of(new ProcessingException(
            ProcessingMessage.withMessage(new I18nMessage("msg-A1")).build(),
            new Exception("Source cause"))),
        Arguments.of(new ProcessingException("msg-A1", (Object[]) null)),
        Arguments.of(new ProcessingException(ProcessingMessage.withMessage("msg-A1").build())),
        Arguments.of(new ProcessingException("msg-A1", new Exception("Source cause"))),
        Arguments.of(
            new ProcessingException("msg-A1", new Throwable("Sample exception"))
                .withMessageType(WARNING)
                .addContextInfo((List<ContextInfo>) null)
                .addContextInfo((ContextInfo[]) null)
        ),
        Arguments.of(new ProcessingException("msg-A1", new Throwable())
            .withMessageType(INFO)
            .withHelpUri("g.com")
            .withStatus(ClientError.CONFLICT)
            .withLogLevel(LogLevel.ERROR)
            .withSecurityLevel(SecurityLevel.CONFIDENTIAL)
            .addContextInfo(ContextInfo.of(NAME)
                .withContext(TYPE, VALUE)
                .withHelp(FORMAT, FORMAT_TYPE)
                .withMessage("Address ''{0}'' already exists in the system",
                    "His Holiness Pope Francis / Vatican City State, 00120")
                .build())),
        Arguments.of(
            new ProcessingException(
                ProcessingMessage
                    .withMessage("msg-A2", 123)
                    .withMessageType(WARNING)
                    .withHelpUri(new URI("i18n.church"))
                    .addContextInfo(
                        ContextInfo.of(NAME).withContext(new ContextValue(TYPE, VALUE))
                            .withHelp(new ContextValue(FORMAT, FORMAT_TYPE))
                            .withMessage(
                                new I18nMessage("Address ''{0}'' already exists in the system", ""))
                            .build(),
                        ContextInfo.of(NAME)
                            .withContext(new ContextValue(VALUE, ValueType.NUMBER))
                            .withHelp(new ContextValue(ValueType.STRING, ValueType.XML))
                            .build(),
                        ContextInfo.of(NAME).withContext(VALUE)
                            .withHelp(new ContextValue(ValueType.ENUM, ValueType.XML))
                            .build(),
                        ContextInfo.of(NAME).withContext("ASD", VALUE).build(),

                        ContextInfo.of(null).build(),
                        ContextInfo.of(NAME).withMessage(new I18nMessage("msg")).build(),
                        ContextInfo.of(NAME).withHelp(new ContextValue(FORMAT))
                            .build(),
                        ContextInfo.builder(COPY_BUILDER_CONTEXT_INFO).build()
                    )
                    .addContextInfo(COPY_BUILDER_CONTEXT_INFO)
                    .build(), new Throwable())
        ),
        Arguments.of(
            new ProcessingException("msg-A1")
                .withHelpUri(new URI("i18n.church"))
                .addContextInfo(
                    List.of(ContextInfo.of(NAME)
                        .withContext(new ContextValue(TYPE, VALUE)).build())
                )
                .addContextInfo(ContextInfo.of(NAME).build())
        )
    );
  }

  @Test
  void testLogLevel_customValue() {
    ProcessingException exception = new ProcessingException("msg-A1").withLogLevel(LogLevel.INFO);
    assertEquals(LogLevel.INFO, exception.getLogLevel());
  }

  @Test
  void testLogLevel_defaultValue() {
    ProcessingException exception = new ProcessingException("msg-A1");
    assertEquals(LogLevel.MESSAGE_TYPE_MAPPING, exception.getLogLevel());
  }

  @ParameterizedTest
  @MethodSource("exceptionSources")
  void testNoException(final ProcessingException exception) {
    //so far now. When there will be a need, I'll improve them. Bigger fish to fry now..
    assertTrue(exception.toString().startsWith("ProcessingException"));
  }

  @Test
  void testStatus_customValue() {
    ProcessingException exception = new ProcessingException("msg-A1")
        .withStatus(ClientError.BAD_REQUEST);
    assertEquals(ClientError.BAD_REQUEST.getStatusId(),
        exception.getStatus().getStatusId());
  }

  @Test
  void testStatus_defaultValue() {
    ProcessingException exception = new ProcessingException("msg-A1");
    assertEquals(DefaultStatus.UNKNOWN, exception.getStatus());
  }
}
