/*
 * Copyright (c) 2024 Juraj Jurčo
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package church.i18n.processing.builder;

import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertSame;

import church.i18n.processing.message.ContextInfoBuilder;
import church.i18n.processing.message.ContextInfoNoopBuilder;
import church.i18n.processing.message.ContextValue;
import church.i18n.processing.message.I18nMessage;
import church.i18n.processing.message.ValueType;
import church.i18n.processing.security.policy.SecurityLevel;
import org.junit.jupiter.api.Test;

class ContextInfoNoopBuilderTest {


  @Test
  void expectedSameInstance() {
    ContextInfoBuilder inst = ContextInfoNoopBuilder.NO_OP_CONTEXT_INFO_BUILDER;

    assertSame(inst, inst.withContext("as"), "Not returns itself");
    assertSame(inst, inst.withContext(new ContextValue("as", "as")), "Not returns itself");
    assertSame(inst, inst.withContext("as", "as"), "Not returns itself");
    assertSame(inst, inst.withContext("as", ValueType.DATETIME), "Not returns itself");

    assertSame(inst, inst.withHelp("as"), "Not returns itself");
    assertSame(inst, inst.withHelp(new ContextValue("as", "as")), "Not returns itself");
    assertSame(inst, inst.withHelp("as", "as"), "Not returns itself");
    assertSame(inst, inst.withHelp("as", ValueType.DATETIME), "Not returns itself");

    assertSame(inst, inst.withMessage("as", "as"), "Not returns itself");
    assertSame(inst, inst.withMessage(new I18nMessage("as", "as")), "Not returns itself");
    assertSame(inst, inst.withSecurityLevel(SecurityLevel.THIRD_PARTY), "Not returns itself");

    //Build returns always null even after call everything...
    assertNull(inst.build());
  }
}
