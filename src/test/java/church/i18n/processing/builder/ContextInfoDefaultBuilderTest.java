/*
 * Copyright (c) 2024 Juraj Jurčo
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package church.i18n.processing.builder;

import static church.i18n.processing.message.CommonContextValues.DOUBLE_REGEX;
import static church.i18n.processing.message.CommonContextValues.INTEGER_REGEX;
import static church.i18n.processing.security.policy.SecurityLevel.CONFIDENTIAL;
import static church.i18n.processing.security.policy.SecurityLevel.RESTRICTED;
import static church.i18n.processing.security.policy.SecurityLevel.SYSTEM_INTERNAL;
import static org.junit.jupiter.api.Assertions.assertEquals;

import church.i18n.processing.message.ContextInfo;
import church.i18n.processing.message.ContextInfoDefaultBuilder;
import church.i18n.processing.message.ContextValue;
import church.i18n.processing.message.I18nMessage;
import church.i18n.processing.message.ValueType;
import org.junit.jupiter.api.Test;

class ContextInfoDefaultBuilderTest {

  @Test
  void build_allFields1() {
    final ContextInfo actual = new ContextInfoDefaultBuilder("build_allFields1")
        .withContext(1)
        .withHelp(2.0)
        .withMessage("error-code")
        .withSecurityLevel(SYSTEM_INTERNAL)
        .build();
    final ContextInfo expected = new ContextInfo("build_allFields1", new ContextValue(1),
        new ContextValue(2.0), new I18nMessage("error-code"), SYSTEM_INTERNAL);
    assertEquals(actual, expected, "ContextInfo instances does not match!");
  }

  @Test
  void build_allFields2() {
    final ContextInfo actual = new ContextInfoDefaultBuilder("build_allFields2")
        .withContext(INTEGER_REGEX)
        .withHelp(DOUBLE_REGEX)
        .withMessage("error-code", 1)
        .withSecurityLevel(CONFIDENTIAL)
        .build();
    final ContextInfo expected = new ContextInfo("build_allFields2", INTEGER_REGEX, DOUBLE_REGEX,
        new I18nMessage("error-code", 1), CONFIDENTIAL);
    assertEquals(actual, expected, "ContextInfo instances does not match!");
  }

  @Test
  void build_allFields3() {
    final ContextInfo actual = new ContextInfoDefaultBuilder("build_allFields3")
        .withContext("context", "contextType")
        .withHelp("help", "helpType")
        .withMessage("error-code", (Object) null)
        .withSecurityLevel(RESTRICTED)
        .build();
    final ContextInfo expected = new ContextInfo("build_allFields3",
        new ContextValue("context", "contextType"),
        new ContextValue("help", "helpType"),
        new I18nMessage("error-code", (Object) null), RESTRICTED);
    assertEquals(actual, expected, "ContextInfo instances does not match!");
  }

  @Test
  void build_allFields4() {
    final ContextInfo actual = new ContextInfoDefaultBuilder("build_allFields4")
        .withContext("context", ValueType.URL)
        .withHelp("help", ValueType.URI)
        .withMessage("error-code", (Object[]) null)
        .withSecurityLevel(RESTRICTED)
        .build();
    final ContextInfo expected = new ContextInfo("build_allFields4",
        new ContextValue("context", ValueType.URL),
        new ContextValue("help", ValueType.URI),
        new I18nMessage("error-code", (Object[]) null), RESTRICTED);
    assertEquals(actual, expected, "ContextInfo instances does not match!");
  }

  @Test
  void build_constructorOnly() {
    final ContextInfo actual = new ContextInfoDefaultBuilder("name").build();
    final ContextInfo expected = new ContextInfo("name", null, null, null, null);
    assertEquals(actual, expected, "ContextInfo instances does not match!");
  }

}
