/*
 * Copyright (c) 2024 Juraj Jurčo
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package church.i18n.processing.logger;

import static church.i18n.processing.security.policy.SecurityLevel.CONFIDENTIAL;
import static church.i18n.processing.security.policy.SecurityLevel.RESTRICTED;
import static church.i18n.processing.security.policy.SecurityLevel.SYSTEM_INTERNAL;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.spi.ILoggingEvent;
import church.i18n.processing.config.DefaultProcessingExceptionConfig;
import church.i18n.processing.config.ProcessingExceptionConfig;
import church.i18n.processing.exception.ProcessingException;
import church.i18n.processing.message.MessageSeverity;
import church.i18n.processing.message.MessageStatus;
import church.i18n.processing.message.ProcessingMessage;
import church.i18n.processing.security.sanitizer.SecurityInfoSanitizer;
import church.i18n.test.LogTracker;
import church.i18n.test.LogTrackerStub;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Stream;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.RegisterExtension;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

class MessageSeverityLogMapperTest {

  private static final SecurityInfoSanitizer DENY_ALL = (infoSecurityPolicy, config) -> false;
  private static final SecurityInfoSanitizer ALLOW_ALL = (infoSecurityPolicy, config) -> true;
  private static final ProcessingMessage FAILOVER_MESSAGE = ProcessingMessage.withMessage(
      "failover-msg").build();
  public static final ProcessingExceptionConfig CONFIG = new DefaultProcessingExceptionConfig(
      MessageStatus.INFO,
      SYSTEM_INTERNAL,
      Set.of(CONFIDENTIAL, RESTRICTED),
      Set.of(),
      FAILOVER_MESSAGE
  );
  private static final Map<MessageSeverity, Level> LEVELS_MAP = Map.of(
      MessageSeverity.LOW, Level.TRACE,
      MessageSeverity.MINOR, Level.DEBUG,
      MessageSeverity.NORMAL, Level.INFO,
      MessageSeverity.MODERATE, Level.WARN,
      MessageSeverity.HIGH, Level.WARN,
      MessageSeverity.URGENT, Level.ERROR,
      MessageSeverity.CRITICAL, Level.ERROR
  );
  private final MessageSeverityLogMapper mapper = new MessageSeverityLogMapper(ALLOW_ALL, CONFIG);
  private final ProcessingException exception = new ProcessingException("log-debug");
  @RegisterExtension
  final
  LogTrackerStub logTrackerStubDebug = LogTrackerStub.create()
      .recordForLevel(LogTracker.LogLevel.TRACE)
      .recordForType(MessageSeverityLogMapper.class);

  @ParameterizedTest
  @MethodSource("getLevelMaps")
  void log_sanitized_debug(final MessageSeverity severity, final Level level) {
    mapper.log(exception.withMessageType(severity));

    final List<ILoggingEvent> events = logTrackerStubDebug.getLoggingEventsForLogLevel(level);
    assertEquals(1, events.size());
    final ILoggingEvent event = events.getFirst();
    assertEquals(level, event.getLevel());
    assertEquals("log-debug", event.getMessage());
  }

  @Test
  void log_otherClass() {
    mapper.log(exception.withMessageType(MessageStatus.WARNING));

    final List<ILoggingEvent> events = logTrackerStubDebug.getLoggingEventsForLogLevel(Level.ERROR);
    assertEquals(2, events.size());
    ILoggingEvent event = events.getFirst();
    assertEquals(Level.ERROR, event.getLevel());
    assertTrue(event.getFormattedMessage()
        .startsWith("This log mapper is unable to handle 'WARNING' message type."));
    event = events.get(1);
    assertEquals(Level.ERROR, event.getLevel());
    assertEquals("log-debug", event.getMessage());
  }

  private static Stream<Arguments> getLevelMaps() {
    return LEVELS_MAP.entrySet().stream()
        .map(entry -> Arguments.of(entry.getKey(), entry.getValue()));
  }

}
