/*
 * Copyright (c) 2024 Juraj Jurčo
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package church.i18n.processing.logger;

import static church.i18n.processing.security.policy.SecurityLevel.CONFIDENTIAL;
import static church.i18n.processing.security.policy.SecurityLevel.RESTRICTED;
import static church.i18n.processing.security.policy.SecurityLevel.SYSTEM_INTERNAL;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.clearInvocations;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import ch.qos.logback.classic.Level;
import church.i18n.processing.config.DefaultProcessingExceptionConfig;
import church.i18n.processing.config.ProcessingExceptionConfig;
import church.i18n.processing.exception.ProcessingException;
import church.i18n.processing.message.MessageSeverity;
import church.i18n.processing.message.MessageStatus;
import church.i18n.processing.message.MessageType;
import church.i18n.processing.message.ProcessingMessage;
import church.i18n.processing.security.sanitizer.SecurityInfoSanitizer;
import java.util.Map;
import java.util.Set;
import java.util.stream.Stream;
import org.jetbrains.annotations.NotNull;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class MessageTypeLogMapperTest {

  private static final SecurityInfoSanitizer DENY_ALL = (infoSecurityPolicy, config) -> false;
  private static final SecurityInfoSanitizer ALLOW_ALL = (infoSecurityPolicy, config) -> true;
  private static final ProcessingMessage FAILOVER_MESSAGE = ProcessingMessage.withMessage(
      "failover-msg").build();
  private static final ProcessingExceptionConfig CONFIG = new DefaultProcessingExceptionConfig(
      MessageStatus.INFO,
      SYSTEM_INTERNAL,
      Set.of(CONFIDENTIAL, RESTRICTED),
      Set.of(),
      FAILOVER_MESSAGE
  );
  private final ProcessingException exception = new ProcessingException("log-debug");
  public final MessageStatusLogMapper statusMapper = spy(new MessageStatusLogMapper(ALLOW_ALL, CONFIG));
  public final MessageSeverityLogMapper severityMapper = spy(new MessageSeverityLogMapper(ALLOW_ALL,
      CONFIG));
  private final MessageTypeLogMapper mapper = new MessageTypeLogMapper(Map.of(
      MessageStatus.class, this.statusMapper,
      MessageSeverity.class, this.severityMapper
  ));

  @ParameterizedTest
  @MethodSource("getLevelMaps")
  void log_sanitized(final MessageType type, final Level level) {
    this.mapper.log(this.exception.withMessageType(type));

    if (type instanceof MessageStatus) {
      verify(this.statusMapper, times(1)).log(any());
      clearInvocations(this.statusMapper);
    }
    if (type instanceof MessageSeverity) {
      verify(this.severityMapper, times(1)).log(any());
      clearInvocations(this.severityMapper);
    }
    if (type instanceof MessageTypeTest) {
      verify(this.severityMapper, times(0)).log(any());
      verify(this.statusMapper, times(0)).log(any());
    }
  }

  private static Stream<Arguments> getLevelMaps() {
    return Stream.of(
        Arguments.of(MessageStatus.ERROR, Level.ERROR),
        Arguments.of(MessageStatus.INFO, Level.INFO),
        Arguments.of(MessageStatus.WARNING, Level.WARN),
        Arguments.of(MessageSeverity.LOW, Level.TRACE),
        Arguments.of(MessageSeverity.MINOR, Level.DEBUG),
        Arguments.of(MessageSeverity.NORMAL, Level.INFO),
        Arguments.of(MessageSeverity.MODERATE, Level.WARN),
        Arguments.of(MessageSeverity.HIGH, Level.WARN),
        Arguments.of(MessageSeverity.URGENT, Level.ERROR),
        Arguments.of(MessageSeverity.CRITICAL, Level.ERROR),
        Arguments.of(MessageTypeTest.LALA, Level.ERROR)
    );
  }

  private enum MessageTypeTest implements MessageType {
    LALA;

    @Override
    public @NotNull String getType() {
      return "LALA";
    }
  }


}
