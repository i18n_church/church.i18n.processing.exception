/*
 * Copyright (c) 2024 Juraj Jurčo
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package church.i18n.processing.config;

import static church.i18n.processing.message.MessageStatus.ERROR;
import static church.i18n.processing.message.MessageStatus.INFO;
import static church.i18n.processing.security.policy.SecurityLevel.CONFIDENTIAL;
import static church.i18n.processing.security.policy.SecurityLevel.PUBLIC;
import static church.i18n.processing.security.policy.SecurityLevel.RESTRICTED;
import static church.i18n.processing.security.policy.SecurityLevel.SYSTEM_EXTERNAL;
import static church.i18n.processing.security.policy.SecurityLevel.SYSTEM_INTERNAL;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import church.i18n.processing.message.MessageType;
import church.i18n.processing.message.ProcessingMessage;
import church.i18n.processing.security.policy.SecurityLevel;
import church.i18n.processing.security.policy.SecurityPolicy;
import java.util.Set;
import org.junit.jupiter.api.Test;

class DefaultProcessingExceptionConfigTest {

  @Test
  void getSecurityLevels_custom() {
    DefaultProcessingExceptionConfig config = new DefaultProcessingExceptionConfig(ERROR,
        SecurityLevel.RESTRICTED, Set.of(CONFIDENTIAL, SYSTEM_INTERNAL), Set.of(CONFIDENTIAL,
        SYSTEM_INTERNAL), new ProcessingMessage("err-g-1"));
    assertEquals(Set.of(CONFIDENTIAL, SYSTEM_INTERNAL), config.exposeSecurityPolicies());
  }

  @Test
  void getSecurityLevels_default() {
    DefaultProcessingExceptionConfig config = DefaultProcessingExceptionConfig.builder().build();
    assertEquals(Set.of(PUBLIC, SYSTEM_EXTERNAL), config.exposeSecurityPolicies());
  }

  @Test
  void getSeverity_custom() {
    DefaultProcessingExceptionConfig config = new DefaultProcessingExceptionConfig(INFO,
        RESTRICTED, Set.of(CONFIDENTIAL, SYSTEM_INTERNAL), Set.of(CONFIDENTIAL,
        SYSTEM_INTERNAL), new ProcessingMessage("err-g-1"));
    assertEquals(INFO, config.defaultMessageType());
  }

  @Test
  void getSeverity_default() {
    DefaultProcessingExceptionConfig config = DefaultProcessingExceptionConfig.builder().build();
    assertEquals(ERROR, config.defaultMessageType());
  }

  @Test
  void testToString() {
    DefaultProcessingExceptionConfig config = DefaultProcessingExceptionConfig.builder().build();
    assertTrue(config.toString().contains("{"));
  }

  @Test
  void testBuilder() {
    MessageType messageType = INFO;
    SecurityPolicy securityPolicy = SYSTEM_EXTERNAL;
    Set<SecurityPolicy> exposePolicies = Set.of(CONFIDENTIAL);
    Set<SecurityPolicy> logPolicies = Set.of(RESTRICTED);
    ProcessingMessage failoverMessage = new ProcessingMessage("failover-message");

    DefaultProcessingExceptionConfig actual = DefaultProcessingExceptionConfig.builder()
        .withDefaultMessageType(messageType)
        .withDefaultSecurityPolicy(securityPolicy)
        .withExposeSecurityPolicies(exposePolicies)
        .withFailoverProcessingMessage(failoverMessage)
        .withLogSecurityPolicies(logPolicies)
        .build();
    final DefaultProcessingExceptionConfig expected = new DefaultProcessingExceptionConfig(
        messageType, securityPolicy, exposePolicies, logPolicies, failoverMessage);

    assertEquals(expected, actual);
    assertEquals(expected.defaultMessageType(), actual.defaultMessageType());
    assertEquals(expected.defaultSecurityPolicy(), actual.defaultSecurityPolicy());
    assertEquals(expected.exposeSecurityPolicies(), actual.exposeSecurityPolicies());
    assertEquals(expected.logSecurityPolicies(), actual.logSecurityPolicies());
    assertEquals(expected.failoverProcessingMessage(), actual.failoverProcessingMessage());
  }
}
