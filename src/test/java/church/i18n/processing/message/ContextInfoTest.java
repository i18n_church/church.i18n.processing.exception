/*
 * Copyright (c) 2024 Juraj Jurčo
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package church.i18n.processing.message;

import static church.i18n.processing.message.CommonContextValues.INTEGER_REGEX;
import static church.i18n.processing.message.CommonContextValues.ISO_ZONED_DATE_TIME;
import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

class ContextInfoTest {

  @Test
  void of() {
    assertEquals(
        new ContextInfo("ctx-info", null, null, null, null),
        ContextInfo.of("ctx-info").build()
    );
  }

  @Test
  void of_ctVal() {
    assertEquals(
        new ContextInfo("ctx-info", INTEGER_REGEX, null, null, null),
        ContextInfo.of("ctx-info", INTEGER_REGEX)
    );
  }

  @Test
  void of_ctVal_help_msg() {
    assertEquals(
        new ContextInfo("ctx-info", INTEGER_REGEX, ISO_ZONED_DATE_TIME, new I18nMessage("msg"),
            null),
        ContextInfo.of("ctx-info", INTEGER_REGEX, ISO_ZONED_DATE_TIME, new I18nMessage("msg"))
    );
  }

  @Test
  void of_ctVal_msg() {
    assertEquals(
        new ContextInfo("ctx-info", INTEGER_REGEX, null, new I18nMessage("msg"), null),
        ContextInfo.of("ctx-info", INTEGER_REGEX, new I18nMessage("msg"))
    );
  }
}
