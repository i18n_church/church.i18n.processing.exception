/*
 * Copyright (c) 2024 Juraj Jurčo
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package church.i18n.processing.message;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import org.junit.jupiter.api.Test;

class ContextValueTest {

  @Test
  void fromCollection() {
    final ContextValue value = ContextValue.fromCollection(List.of("value"), ValueType.SET);
    assertEquals(new ContextValue("[value]", ValueType.SET), value);
  }

  @Test
  void fromCollection_empty() {
    final ContextValue value = ContextValue.fromCollection(List.of(), ValueType.LIST);
    assertEquals(new ContextValue("[]", ValueType.LIST), value);
  }

  @Test
  void fromCollection_null() {
    final ContextValue value = ContextValue.fromCollection(null, ValueType.SET);
    assertEquals(new ContextValue(null, ValueType.SET), value);
  }

  @Test
  void fromEnum() {
    final ContextValue value = ContextValue.fromEnum(ValueType.class);
    final String s = Arrays.toString(ValueType.class.getEnumConstants());
    assertEquals(new ContextValue(s, ValueType.ENUM), value);
  }

  @Test
  void fromEnumList() {
    final ContextValue value = ContextValue.fromEnum(ValueType.AUTO_MIME_TYPE,
        ValueType.JSON);
    assertEquals(new ContextValue("[AUTO_MIME_TYPE, JSON]", ValueType.ENUM), value);
  }

  @Test
  void fromEnumList_null() {
    final ContextValue value = ContextValue.fromEnum((Enum<ValueType>[]) null);
    assertEquals(new ContextValue(null, ValueType.ENUM), value);
  }

  @Test
  void fromEnumList_nullValues() {
    final ContextValue value = ContextValue.fromEnum(null, null);
    assertEquals(new ContextValue("[null, null]", ValueType.ENUM), value);
  }

  @Test
  void fromEnum_null() {
    final ContextValue value = ContextValue.fromEnum((Class<ValueType>) null);
    assertEquals(new ContextValue(null, ValueType.ENUM), value);
  }

  @Test
  void fromList() {
    final ContextValue value = ContextValue.fromList(List.of("a", "as"));
    assertEquals(new ContextValue("[a, as]", ValueType.LIST), value);
  }

  @Test
  void fromList_null() {
    final ContextValue value = ContextValue.fromList(null);
    assertEquals(new ContextValue(null, ValueType.LIST), value);
  }

  @Test
  void fromMap() {
    final LinkedHashMap<String, String> map = new LinkedHashMap<>();
    map.put("a", "as");
    map.put("b", "bs");
    final ContextValue value = ContextValue.fromMap(map);
    assertEquals(new ContextValue("{a=as, b=bs}", ValueType.MAP), value);
  }

  @Test
  void fromMap_null() {
    final ContextValue value = ContextValue.fromMap(null);
    assertEquals(new ContextValue(null, ValueType.MAP), value);
  }

  @Test
  void fromSet() {
    final LinkedHashSet<String> set = new LinkedHashSet<>();
    set.add("a");
    set.add("b");
    set.add("c");
    final ContextValue value = ContextValue.fromSet(set);
    assertEquals(new ContextValue("[a, b, c]", ValueType.SET), value);
  }

  @Test
  void fromSet_null() {
    final ContextValue value = ContextValue.fromSet(null);
    assertEquals(new ContextValue(null, ValueType.SET), value);
  }
}
