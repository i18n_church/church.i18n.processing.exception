/*
 * Copyright (c) 2024 Juraj Jurčo
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package church.i18n.processing.message;

import static org.junit.jupiter.api.Assertions.assertEquals;

import church.i18n.processing.storage.ProcessingIdProvider;
import church.i18n.processing.storage.ThreadIndependentStorage;
import java.util.List;
import org.junit.jupiter.api.Test;

class ProcessingMessageDefaultBuilderTest {

  @Test
  void addToMessageStorage() {
    ProcessingIdProvider idProvider = () -> "id";
    ThreadIndependentStorage storage = new ThreadIndependentStorage(idProvider);
    storage.clearStorage(idProvider.getProcessingId());

    new ProcessingMessageDefaultBuilder("err-add-to-storage")
        .addToMessageStorage(storage);

    final List<ProcessingMessage> messages = storage.get(idProvider.getProcessingId());

    assertEquals(1, messages.size());
    assertEquals("err-add-to-storage", messages.getFirst().getMessage().getCode());
  }

  @Test
  void addToMessageStorage_noIdProvider() {
    ProcessingIdProvider idProvider = () -> "id";
    ThreadIndependentStorage storage = new ThreadIndependentStorage(idProvider);
    storage.clearStorage();

    new ProcessingMessageDefaultBuilder("err-add-to-storage")
        .addToMessageStorage(storage);

    final List<ProcessingMessage> messages = storage.get();

    assertEquals(1, messages.size());
    assertEquals("err-add-to-storage", messages.getFirst().getMessage().getCode());
  }
}
