/*
 * Copyright (c) 2024 Juraj Jurčo
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package church.i18n.processing;

import static nl.jqno.equalsverifier.Warning.NONFINAL_FIELDS;
import static nl.jqno.equalsverifier.Warning.STRICT_INHERITANCE;

import church.i18n.processing.config.DefaultProcessingExceptionConfig;
import church.i18n.processing.exception.ProcessingException;
import church.i18n.processing.exception.ProcessingExceptionOptionalBuilder;
import church.i18n.processing.message.ContextInfo;
import church.i18n.processing.message.ContextInfoDefaultBuilder;
import church.i18n.processing.message.ContextValue;
import church.i18n.processing.message.I18nMessage;
import church.i18n.processing.message.ProcessingMessage;
import church.i18n.processing.message.ProcessingMessageAbstractBuilder;
import church.i18n.processing.status.ClientError;
import church.i18n.processing.status.ServerError;
import church.i18n.processing.status.Status;
import nl.jqno.equalsverifier.EqualsVerifier;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

class EqualsAndHashCodeTest {

  @Test
  void testContextValue() {
    EqualsVerifier.forClasses(ContextValue.class, ContextInfo.class, I18nMessage.class,
        ProcessingMessage.class
    ).suppress(STRICT_INHERITANCE)
        .verify();
  }

  @Test
  void testContextInfoDefaultBuilder() {
    EqualsVerifier.forClasses(
        ContextInfoDefaultBuilder.class,
        DefaultProcessingExceptionConfig.class
    ).suppress(STRICT_INHERITANCE, NONFINAL_FIELDS)
        .verify();
  }

  @Test
  void testProcessingExceptionOptionalBuilder() {
    EqualsVerifier.forClasses(
        ProcessingMessageAbstractBuilder.class,
        ProcessingExceptionOptionalBuilder.class
    ).suppress(STRICT_INHERITANCE, NONFINAL_FIELDS)
        .verify();
  }

  @Test
  @Disabled("Seems like the library has problem with modules: https://github.com/jqno/equalsverifier/issues/746")
  void testProcessingException() {
    EqualsVerifier
        .forClass(ProcessingException.class)
        .suppress(NONFINAL_FIELDS, STRICT_INHERITANCE)
        .withPrefabValues(Status.class, ClientError.FORBIDDEN, ServerError.INTERNAL_SERVER_ERROR)
        .withIgnoredFields("detailMessage", "cause", "stackTrace", "suppressedExceptions")
        .verify();
  }

}
