/*
 * Copyright (c) 2024 Juraj Jurčo
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package church.i18n.processing;

import com.openpojo.reflection.PojoClassFilter;
import com.openpojo.reflection.filters.FilterClassName;
import com.openpojo.validation.Validator;
import com.openpojo.validation.ValidatorBuilder;
import com.openpojo.validation.rule.impl.EqualsAndHashCodeMatchRule;
import com.openpojo.validation.rule.impl.GetterMustExistRule;
import com.openpojo.validation.rule.impl.NoNestedClassRule;
import com.openpojo.validation.rule.impl.NoPublicFieldsRule;
import com.openpojo.validation.rule.impl.SerializableMustHaveSerialVersionUIDRule;
import com.openpojo.validation.test.impl.GetterTester;
import com.openpojo.validation.test.impl.SetterTester;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

public class PojoTest {

  final PojoClassFilter TEST_CLASSES_FILTER = new FilterClassName("^((?!(Test|Utils|Builder)$).)*$");

  @Test
  void testException() {
    Validator validator = ValidatorBuilder.create()
        //rules
        .with(new NoNestedClassRule())
        .with(new NoPublicFieldsRule())
        .with(new GetterMustExistRule())
        .with(new EqualsAndHashCodeMatchRule())
        .with(new SerializableMustHaveSerialVersionUIDRule())
        //testers
        .with(new SetterTester())
        .with(new GetterTester())
        .build();

    validator.validate("church.i18n.processing.exception", TEST_CLASSES_FILTER);
  }

  @Test
  void testLoggerPojo() {
    Validator validator = ValidatorBuilder.create()
        //testers
        .with(new GetterTester())
        .build();

    validator.validate("church.i18n.processing.logger", TEST_CLASSES_FILTER);
  }

  @Test
  @Disabled("Flaky: java.lang.reflect.InaccessibleObjectException: Unable to make field transient java.lang.Object[] "
      + "java.util.ArrayDeque.elements accessible: module java.base does not \"opens java.util\" to unnamed module")
  void testModelPojo() {
    Validator validator = ValidatorBuilder.create()
        //rules
        .with(new EqualsAndHashCodeMatchRule())
        .with(new SerializableMustHaveSerialVersionUIDRule())
        //testers
        .with(new SetterTester())
        .with(new GetterTester())
        .build();

    validator.validate("church.i18n.processing.message", TEST_CLASSES_FILTER);
  }

  @Test
  void testSecurityPojo() {
    Validator validator = ValidatorBuilder.create()
        //testers
        .with(new GetterTester())
        .build();

    validator.validate("church.i18n.processing.security", TEST_CLASSES_FILTER);
  }

  @Test
  void testStatusPojo() {
    Validator validator = ValidatorBuilder.create()
        //testers
        .with(new GetterTester())
        .build();

    validator.validate("church.i18n.processing.status", TEST_CLASSES_FILTER);
  }
}
