/*
 * Copyright (c) 2024 Juraj Jurčo
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package church.i18n.processing.security;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import church.i18n.processing.security.policy.SecurityLevel;
import org.jetbrains.annotations.NotNull;
import org.junit.jupiter.api.Test;

class SecurityLevelTest {

  @Test
  void parse_data() {
    final @NotNull SecurityLevel[] levels = SecurityLevel
        .parse("PUbLIC", "SYSTEm_EXTERNAL", "sYSTEM_INTERNAL",
            "THIRD_PARTY", "RESTRICTED", "cONFIDENTIAL");
    assertEquals(6, levels.length);
    assertEquals(SecurityLevel.PUBLIC, levels[0]);
    assertEquals(SecurityLevel.SYSTEM_EXTERNAL, levels[1]);
    assertEquals(SecurityLevel.SYSTEM_INTERNAL, levels[2]);
    assertEquals(SecurityLevel.THIRD_PARTY, levels[3]);
    assertEquals(SecurityLevel.RESTRICTED, levels[4]);
    assertEquals(SecurityLevel.CONFIDENTIAL, levels[5]);
  }

  @Test
  void parse_invalid() {
    assertNotNull(SecurityLevel.parse("null", "sys-tem", ""));
    assertEquals(0, SecurityLevel.parse("private", "non-valid").length, "Expected length is zero.");
  }

  @Test
  void parse_mixed() {
    final @NotNull SecurityLevel[] levels = SecurityLevel
        .parse("PUbLIC", "null", "SYSTEm_EXTERNAL", "", "sYSTEM_INTERNAL", "system-internal",
            "THIRD_PARTY", "a", "RESTRICTED", "@!@#$%", "cONFIDENTIAL", null, null, null,
            "فِي البَدءِ خَلَقَ اللهُ السَّماواتِ وَالأرْضَ.");
    assertEquals(6, levels.length);
    assertEquals(SecurityLevel.PUBLIC, levels[0]);
    assertEquals(SecurityLevel.SYSTEM_EXTERNAL, levels[1]);
    assertEquals(SecurityLevel.SYSTEM_INTERNAL, levels[2]);
    assertEquals(SecurityLevel.THIRD_PARTY, levels[3]);
    assertEquals(SecurityLevel.RESTRICTED, levels[4]);
    assertEquals(SecurityLevel.CONFIDENTIAL, levels[5]);
  }

  @Test
  void parse_none() {
    assertNotNull(SecurityLevel.parse());
    assertEquals(0, SecurityLevel.parse().length, "Expected length is zero");

    assertNotNull(SecurityLevel.parse((String[]) null));
    assertEquals(0, SecurityLevel.parse((String[]) null).length, "Expected length is zero.");

    assertNotNull(SecurityLevel.parse(null, null, null));
    assertEquals(0, SecurityLevel.parse(null, null, null).length, "Expected length is zero.");
  }
}
