/*
 * Copyright (c) 2024 Juraj Jurčo
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package church.i18n.processing.security.sanitizer;

import static church.i18n.processing.message.MessageStatus.INFO;
import static church.i18n.processing.security.policy.SecurityLevel.CONFIDENTIAL;
import static church.i18n.processing.security.policy.SecurityLevel.PUBLIC;
import static church.i18n.processing.security.policy.SecurityLevel.RESTRICTED;
import static church.i18n.processing.security.policy.SecurityLevel.SYSTEM_INTERNAL;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import church.i18n.processing.config.DefaultProcessingExceptionConfig;
import church.i18n.processing.config.ProcessingExceptionConfig;
import church.i18n.processing.exception.ProcessingException;
import church.i18n.processing.message.ContextInfo;
import church.i18n.processing.message.ProcessingMessage;
import java.util.Optional;
import java.util.Set;
import org.junit.jupiter.api.Test;

class DefaultSecurityInfoSanitizerTest {

  private static final SecurityInfoSanitizer DENY_ALL = (infoSecurityPolicy, config) -> false;
  private static final SecurityInfoSanitizer ALLOW_ALL = (infoSecurityPolicy, config) -> true;
  private static final ProcessingMessage FAILOVER_MESSAGE = ProcessingMessage.withMessage(
      "failover-msg").build();

  @Test
  void showInfo_useDefault_hide() {
    final ProcessingExceptionConfig config = new DefaultProcessingExceptionConfig(INFO,
        SYSTEM_INTERNAL,                 //default
        Set.of(CONFIDENTIAL, RESTRICTED),//expose
        Set.of(),                        //log
        FAILOVER_MESSAGE
    );
    DefaultSecurityInfoSanitizer eSanitizer = new DefaultSecurityInfoSanitizer(
        ProcessingExceptionConfig::defaultSecurityPolicy,
        ProcessingExceptionConfig::exposeSecurityPolicies
    );
    assertFalse(eSanitizer.canPublish(null, config));
  }

  @Test
  void showInfo_useDefault_show() {
    final ProcessingExceptionConfig config = new DefaultProcessingExceptionConfig(INFO,
        CONFIDENTIAL,                    //default
        Set.of(CONFIDENTIAL, RESTRICTED),//expose
        Set.of(),                        //log
        FAILOVER_MESSAGE
    );
    DefaultSecurityInfoSanitizer eSanitizer = new DefaultSecurityInfoSanitizer(
        ProcessingExceptionConfig::defaultSecurityPolicy,
        ProcessingExceptionConfig::exposeSecurityPolicies
    );
    assertTrue(eSanitizer.canPublish(null, config));
  }

  @Test
  void showInfo_useDefault_hide_noAllowed() {
    final ProcessingExceptionConfig config = new DefaultProcessingExceptionConfig(INFO,
        CONFIDENTIAL,                    //default
        Set.of(CONFIDENTIAL, RESTRICTED),//expose
        Set.of(),                        //log
        FAILOVER_MESSAGE
    );
    DefaultSecurityInfoSanitizer lSanitizer = new DefaultSecurityInfoSanitizer(
        ProcessingExceptionConfig::defaultSecurityPolicy,
        ProcessingExceptionConfig::logSecurityPolicies
    );
    assertFalse(lSanitizer.canPublish(null, config));
  }

  @Test
  void sanitize_partialContextInfo() {
    final ProcessingExceptionConfig config = new DefaultProcessingExceptionConfig(INFO,
        SYSTEM_INTERNAL,                    //default
        Set.of(SYSTEM_INTERNAL, RESTRICTED),//expose
        Set.of(),                           //log
        FAILOVER_MESSAGE
    );
    DefaultSecurityInfoSanitizer eSanitizer = new DefaultSecurityInfoSanitizer(
        ProcessingExceptionConfig::defaultSecurityPolicy,
        ProcessingExceptionConfig::exposeSecurityPolicies
    );
    final ProcessingException sanitize = new ProcessingException("test-message")
        .addContextInfo(ContextInfo.of("no-sec-info").build())
        .addContextInfo(ContextInfo.of("not-allowed").withSecurityLevel(PUBLIC).build())
        .addContextInfo(ContextInfo.of("allowed").withSecurityLevel(SYSTEM_INTERNAL).build())
        .withSecurityLevel(RESTRICTED);
    final ProcessingException expected = new ProcessingException("test-message")
        .addContextInfo(ContextInfo.of("no-sec-info").build())
        .addContextInfo(ContextInfo.of("allowed").withSecurityLevel(SYSTEM_INTERNAL).build())
        .withSecurityLevel(RESTRICTED);
    final Optional<ProcessingException> sanitized = eSanitizer.sanitize(sanitize, config);
    assertTrue(sanitized.isPresent());
    assertEquals(expected, sanitized.get());
  }

  @Test
  void sanitize_partialContextInfo_defaultNotAllowed() {
    final ProcessingExceptionConfig config = new DefaultProcessingExceptionConfig(INFO,
        CONFIDENTIAL,                       //default
        Set.of(SYSTEM_INTERNAL, RESTRICTED),//expose
        Set.of(),                           //log
        FAILOVER_MESSAGE
    );
    DefaultSecurityInfoSanitizer eSanitizer = new DefaultSecurityInfoSanitizer(
        ProcessingExceptionConfig::defaultSecurityPolicy,
        ProcessingExceptionConfig::exposeSecurityPolicies
    );
    final ProcessingException sanitize = new ProcessingException("test-message")
        .addContextInfo(ContextInfo.of("no-sec-info").build())
        .addContextInfo(ContextInfo.of("not-allowed").withSecurityLevel(PUBLIC).build())
        .addContextInfo(ContextInfo.of("allowed").withSecurityLevel(SYSTEM_INTERNAL).build())
        .withSecurityLevel(RESTRICTED);
    final ProcessingException expected = new ProcessingException("test-message")
        .addContextInfo(ContextInfo.of("allowed").withSecurityLevel(SYSTEM_INTERNAL).build())
        .withSecurityLevel(RESTRICTED);
    final Optional<ProcessingException> sanitized = eSanitizer.sanitize(sanitize, config);
    assertTrue(sanitized.isPresent());
    assertEquals(expected, sanitized.get());
  }
}
