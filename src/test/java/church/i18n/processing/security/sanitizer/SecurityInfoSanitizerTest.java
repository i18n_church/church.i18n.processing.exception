/*
 * Copyright (c) 2024 Juraj Jurčo
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package church.i18n.processing.security.sanitizer;

import static church.i18n.processing.security.policy.SecurityLevel.CONFIDENTIAL;
import static church.i18n.processing.security.policy.SecurityLevel.RESTRICTED;
import static church.i18n.processing.security.policy.SecurityLevel.SYSTEM_INTERNAL;
import static org.junit.jupiter.api.Assertions.assertEquals;

import church.i18n.processing.config.DefaultProcessingExceptionConfig;
import church.i18n.processing.config.ProcessingExceptionConfig;
import church.i18n.processing.exception.ProcessingException;
import church.i18n.processing.message.ContextInfo;
import church.i18n.processing.message.MessageStatus;
import church.i18n.processing.message.ProcessingMessage;
import java.util.Optional;
import java.util.Set;
import org.junit.jupiter.api.Test;

class SecurityInfoSanitizerTest {

  private static final SecurityInfoSanitizer DENY_ALL = (infoSecurityPolicy, config) -> false;
  private static final SecurityInfoSanitizer ALLOW_ALL = (infoSecurityPolicy, config) -> true;
  private static final ProcessingMessage FAILOVER_MESSAGE = ProcessingMessage.withMessage(
      "failover-msg").build();
  public static final ProcessingExceptionConfig CONFIG = new DefaultProcessingExceptionConfig(
      MessageStatus.INFO,
      SYSTEM_INTERNAL,
      Set.of(CONFIDENTIAL, RESTRICTED),
      Set.of(),
      FAILOVER_MESSAGE
  );

  @Test
  void testSanitizeContextInfo() {
    final ContextInfo noSecInfo = ContextInfo.of("a").build();
    final ContextInfo secInfo = ContextInfo.of("a").withSecurityLevel(SYSTEM_INTERNAL)
        .build();
    assertEquals(Optional.of(noSecInfo), ALLOW_ALL.sanitize(noSecInfo, CONFIG));
    assertEquals(Optional.empty(), DENY_ALL.sanitize(noSecInfo, CONFIG));
    assertEquals(Optional.of(secInfo), ALLOW_ALL.sanitize(secInfo, CONFIG));
    assertEquals(Optional.empty(), DENY_ALL.sanitize(secInfo, CONFIG));
  }

  @Test
  void testSanitizeMessage() {
    final ProcessingMessage noSecInfo = ProcessingMessage.withMessage("test-message").build();
    final ProcessingMessage secInfo = ProcessingMessage.withMessage("test-message")
        .withSecurityLevel(SYSTEM_INTERNAL).build();

    assertEquals(Optional.of(noSecInfo), ALLOW_ALL.sanitize(noSecInfo, CONFIG));
    assertEquals(Optional.empty(), DENY_ALL.sanitize(noSecInfo, CONFIG));
    assertEquals(Optional.of(secInfo), ALLOW_ALL.sanitize(secInfo, CONFIG));
    assertEquals(Optional.empty(), DENY_ALL.sanitize(secInfo, CONFIG));

    final ContextInfo contextInfo = ContextInfo.of("a").build();
    final ProcessingMessage secInfoWithCI = ProcessingMessage.withMessage("test-message")
        .addContextInfo(contextInfo).withSecurityLevel(SYSTEM_INTERNAL).build();

    assertEquals(Optional.of(secInfoWithCI), ALLOW_ALL.sanitize(secInfoWithCI, CONFIG));
    assertEquals(Optional.empty(), DENY_ALL.sanitize(secInfoWithCI, CONFIG));
  }

  @Test
  void sanitizeExConf() {
    final ProcessingException noSecInfo = new ProcessingException("test-message");
    final ProcessingException secInfo = new ProcessingException("test-message")
        .withSecurityLevel(SYSTEM_INTERNAL);

    assertEquals(Optional.of(noSecInfo), ALLOW_ALL.sanitize(noSecInfo, CONFIG));
    assertEquals(Optional.empty(), DENY_ALL.sanitize(noSecInfo, CONFIG));
    assertEquals(Optional.of(secInfo), ALLOW_ALL.sanitize(secInfo, CONFIG));
    assertEquals(Optional.empty(), DENY_ALL.sanitize(secInfo, CONFIG));

    final ContextInfo contextInfo = ContextInfo.of("a").build();
    final ProcessingException secInfoWithCI = new ProcessingException("test-message")
        .addContextInfo(contextInfo).withSecurityLevel(SYSTEM_INTERNAL);

    assertEquals(Optional.of(secInfoWithCI), ALLOW_ALL.sanitize(secInfoWithCI, CONFIG));
    assertEquals(Optional.empty(), DENY_ALL.sanitize(secInfoWithCI, CONFIG));
  }
}
