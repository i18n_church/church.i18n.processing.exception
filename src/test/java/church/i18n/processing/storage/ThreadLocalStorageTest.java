/*
 * Copyright (c) 2024 Juraj Jurčo
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package church.i18n.processing.storage;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

import church.i18n.processing.message.ProcessingMessage;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import org.jetbrains.annotations.NotNull;
import org.junit.jupiter.api.Test;

class ThreadLocalStorageTest {

  @Test
  void testMessagesAreThreadLocal() throws InterruptedException {

    ProcessingIdProvider rip = new ThreadNameProcessingIdProvider();
    ThreadLocalStorage threadLocalStorage = new ThreadLocalStorage(rip);

    int threads = 100;
    int multiplier = 7;
    try (ExecutorService executor = Executors.newFixedThreadPool(threads)) {
      List<Callable<List<ProcessingMessage>>> tasks = new ArrayList<>();

      for (int i = 0; i < threads; i++) {
        int count = i * multiplier;
        tasks.add(() -> {
          for (int j = 0; j < count; j++) {
            String message = "Thread: " + count + " Msg: " + j;
            threadLocalStorage.addMessages(rip.getProcessingId(), new ProcessingMessage(message));
          }
          //messages have not been cleared yet
          assertEquals(threadLocalStorage.get(rip.getProcessingId()),
              threadLocalStorage.get(rip.getProcessingId()));
          List<ProcessingMessage> response = threadLocalStorage.getAndClear(rip.getProcessingId());
          //when we clear them, nothing has left there
          assertEquals(0, threadLocalStorage.get(rip.getProcessingId()).size());
          //what is returned has the size as expected. No other thread has added it's own stuff
          assertEquals(count, response.size());
          return response;
        });
      }
      List<Future<List<ProcessingMessage>>> futures = executor.invokeAll(tasks);
      executor.shutdown();
      if (executor.awaitTermination(1, TimeUnit.MINUTES)) {
        //here we check whether each thread has contained it's own messages
        futures.forEach(future -> {
          if (future.isDone()) {
            try {
              List<ProcessingMessage> ProcessingMessages = future.get();
              int count = ProcessingMessages.size();
              assertEquals(0,
                  count % multiplier);//all lists contains number of messages multiplied by 7
              for (int i = 0; i < count; i++) {
                String expectedMessage =
                    "Thread: " + count + " Msg: " + i;//all messages match and are in expected order
                assertEquals(expectedMessage, ProcessingMessages.get(i).getMessage().getCode());
              }
            } catch (Exception e) {
              fail();
            }
          } else {
            fail();
          }
        });
      } else {
        fail();
      }
    }
  }

  @Test
  void testAddListOfMessagesAndClearMessages() throws InterruptedException {

    ProcessingIdProvider rip = new ThreadNameProcessingIdProvider();
    ThreadLocalStorage threadLocalStorage = new ThreadLocalStorage(rip);

    int threads = 100;
    int multiplier = 7;
    try (ExecutorService executor = Executors.newFixedThreadPool(threads)) {
      List<Callable<List<ProcessingMessage>>> tasks = getCallablesAddList(threads, multiplier, threadLocalStorage);

      List<Future<List<ProcessingMessage>>> futures = executor.invokeAll(tasks);
      executor.shutdown();
      if (executor.awaitTermination(1, TimeUnit.MINUTES)) {
        //here we check whether each thread has contained its own messages
        futures.forEach(future -> {
          if (future.isDone()) {
            try {
              List<ProcessingMessage> ProcessingMessages = future.get();
              int count = ProcessingMessages.size();
              assertEquals(0, count % multiplier);//all lists contain number of messages multiplied by 7
              for (int i = 0; i < count; i++) {
                String expectedMessage =
                    "Thread: " + count + " Msg: " + i;//all messages match and are in expected order
                assertEquals(expectedMessage, ProcessingMessages.get(i).getMessage().getCode());
              }
            } catch (Exception e) {
              fail();
            }
          } else {
            fail();
          }
        });
      } else {
        fail();
      }
    }

  }

  @Test
  void testMessagesAreThreadLocal_noProcessingId() throws InterruptedException {

    ProcessingIdProvider rip = new ThreadNameProcessingIdProvider();
    ThreadLocalStorage threadLocalStorage = new ThreadLocalStorage(rip);

    int threads = 100;
    int multiplier = 7;
    try (ExecutorService executor = Executors.newFixedThreadPool(threads)) {
      List<Callable<List<ProcessingMessage>>> tasks = getCallables(threads, multiplier, threadLocalStorage);

      List<Future<List<ProcessingMessage>>> futures = executor.invokeAll(tasks);
      executor.shutdown();
      if (executor.awaitTermination(1, TimeUnit.MINUTES)) {
        //here we check whether each thread has contained its own messages
        futures.forEach(future -> {
          if (future.isDone()) {
            try {
              List<ProcessingMessage> ProcessingMessages = future.get();
              int count = ProcessingMessages.size();
              assertEquals(0, count % multiplier);//all lists contain number of messages multiplied by 7
              for (int i = 0; i < count; i++) {
                String expectedMessage =
                    "Thread: " + count + " Msg: " + i;//all messages match and are in expected order
                assertEquals(expectedMessage, ProcessingMessages.get(i).getMessage().getCode());
              }
            } catch (Exception e) {
              fail();
            }
          } else {
            fail();
          }
        });
      } else {
        fail();
      }
    }
  }

  private static @NotNull List<Callable<List<ProcessingMessage>>> getCallables(final int threads, final int multiplier,
      final ThreadLocalStorage threadLocalStorage) {
    List<Callable<List<ProcessingMessage>>> tasks = new ArrayList<>();

    for (int i = 0; i < threads; i++) {
      int count = i * multiplier;
      tasks.add(() -> {
        for (int j = 0; j < count; j++) {
          String message = "Thread: " + count + " Msg: " + j;
          threadLocalStorage.addMessages(new ProcessingMessage(message));
        }
        //messages have not been cleared yet
        assertEquals(threadLocalStorage.get(),
            threadLocalStorage.get());
        List<ProcessingMessage> response = threadLocalStorage.getAndClear();
        //when we clear them, nothing has left there
        assertEquals(0, threadLocalStorage.get().size());
        //what is returned has the size as expected. No other thread has added it's own stuff
        assertEquals(count, response.size());
        return response;
      });
    }
    return tasks;
  }

  private static @NotNull List<Callable<List<ProcessingMessage>>> getCallablesAddList(final int threads,
      final int multiplier, final ThreadLocalStorage threadLocalStorage) {
    List<Callable<List<ProcessingMessage>>> tasks = new ArrayList<>();

    for (int i = 0; i < threads; i++) {
      int count = i * multiplier;
      tasks.add(() -> {
        for (int j = 0; j < count; j++) {
          String message = "Thread: " + count + " Msg: " + j;
          //We add list of messages
          threadLocalStorage.addMessages(List.of(new ProcessingMessage(message)));
        }
        //messages have not been cleared yet
        assertEquals(threadLocalStorage.get(), threadLocalStorage.get());
        //we retrieve messages and then we clear them.
        List<ProcessingMessage> response = threadLocalStorage.get();
        threadLocalStorage.clearStorage();
        //when we clear them, nothing has left there
        assertEquals(0, threadLocalStorage.get().size());
        //what is returned has the size as expected. No other thread has added it's own stuff
        assertEquals(count, response.size());
        return response;
      });
    }
    return tasks;
  }
}
