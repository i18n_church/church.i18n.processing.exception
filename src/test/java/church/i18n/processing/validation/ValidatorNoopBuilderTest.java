/*
 * Copyright (c) 2024 Juraj Jurčo
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package church.i18n.processing.validation;

import static church.i18n.processing.exception.ProcessingExceptionNoopBuilder.NO_OP_PROCESSING_EXCEPTION_BUILDER;
import static church.i18n.processing.message.ContextInfoNoopBuilder.NO_OP_CONTEXT_INFO_BUILDER;
import static church.i18n.processing.message.ProcessingMessageNoopBuilder.NO_OP_PROCESSING_MESSAGE_BUILDER;
import static church.i18n.processing.validation.ValidatorNoopBuilder.NO_OP_VALIDATOR_BUILDER;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertSame;

import church.i18n.processing.exception.ProcessingException;
import church.i18n.processing.message.CommonContextValues;
import church.i18n.processing.message.I18nMessage;
import church.i18n.processing.message.ProcessingMessage;
import java.util.Optional;
import org.junit.jupiter.api.Test;

class ValidatorNoopBuilderTest {

  @Test
  void buildContextInfo() {
    assertSame(NO_OP_CONTEXT_INFO_BUILDER, NO_OP_VALIDATOR_BUILDER.buildContextInfo(""));
  }

  @Test
  void buildException() {
    assertSame(NO_OP_PROCESSING_EXCEPTION_BUILDER, NO_OP_VALIDATOR_BUILDER.buildException(""));
    assertSame(NO_OP_PROCESSING_EXCEPTION_BUILDER,
        NO_OP_VALIDATOR_BUILDER.buildException(new ProcessingMessage("")));
    assertSame(NO_OP_PROCESSING_EXCEPTION_BUILDER,
        NO_OP_VALIDATOR_BUILDER.buildException(new ProcessingMessage(""), new Throwable()));
  }

  @Test
  void buildProcessingMessage() {
    assertSame(NO_OP_PROCESSING_MESSAGE_BUILDER,
        NO_OP_VALIDATOR_BUILDER.buildProcessingMessage(""));
    assertSame(NO_OP_PROCESSING_MESSAGE_BUILDER,
        NO_OP_VALIDATOR_BUILDER.buildProcessingMessage(new I18nMessage("code")));
  }

  @Test
  void createContextInfo() {
    assertSame(Optional.empty(), NO_OP_VALIDATOR_BUILDER.createContextInfo("name",
        CommonContextValues.DATE));
    assertSame(Optional.empty(), NO_OP_VALIDATOR_BUILDER.createContextInfo("name",
        CommonContextValues.DATE, new I18nMessage("msg")));
  }

  @Test
  void createException() {
    assertSame(Optional.empty(), NO_OP_VALIDATOR_BUILDER.createException(""));
    assertSame(Optional.empty(),
        NO_OP_VALIDATOR_BUILDER.createException(new ProcessingMessage("")));
    assertSame(Optional.empty(),
        NO_OP_VALIDATOR_BUILDER.createException(new ProcessingMessage(""), new Throwable()));
  }

  @Test
  void createMessage() {
    assertSame(Optional.empty(), NO_OP_VALIDATOR_BUILDER.createMessage("code"));
    assertSame(Optional.empty(), NO_OP_VALIDATOR_BUILDER.createMessage(new I18nMessage("code")));
  }

  @Test
  void throwException() {
    assertDoesNotThrow(() -> NO_OP_VALIDATOR_BUILDER.throwException(""));
    assertDoesNotThrow(() -> NO_OP_VALIDATOR_BUILDER.throwException(new ProcessingMessage("")));
    assertDoesNotThrow(() -> NO_OP_VALIDATOR_BUILDER.throwException(new ProcessingException("")));
    assertDoesNotThrow(() -> NO_OP_VALIDATOR_BUILDER.throwException(new ProcessingMessage(""),
        new Throwable()));
  }
}
