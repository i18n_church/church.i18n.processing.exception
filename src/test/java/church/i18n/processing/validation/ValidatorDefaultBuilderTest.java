/*
 * Copyright (c) 2024 Juraj Jurčo
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package church.i18n.processing.validation;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import church.i18n.processing.exception.ProcessingException;
import church.i18n.processing.exception.ProcessingExceptionOptionalBuilder;
import church.i18n.processing.message.CommonContextValues;
import church.i18n.processing.message.ContextInfo;
import church.i18n.processing.message.ContextInfoDefaultBuilder;
import church.i18n.processing.message.I18nMessage;
import church.i18n.processing.message.ProcessingMessage;
import church.i18n.processing.message.ProcessingMessageOptionalBuilder;
import java.util.Optional;
import org.junit.jupiter.api.Test;

class ValidatorDefaultBuilderTest {

  @Test
  void buildContextInfo() {
    assertEquals(new ContextInfoDefaultBuilder("pc1"),
        new ValidatorDefaultBuilder().buildContextInfo("pc1"));
  }

  @Test
  void buildException() {
    assertEquals(
        new ProcessingExceptionOptionalBuilder(new ProcessingMessage("pe1")).build(),
        new ValidatorDefaultBuilder().buildException("pe1").build()
    );
    assertEquals(
        new ProcessingExceptionOptionalBuilder(new ProcessingMessage("pe2")).build(),
        new ValidatorDefaultBuilder().buildException(new ProcessingMessage("pe2")).build()
    );
    Throwable cause = new Throwable();
    assertEquals(
        new ProcessingExceptionOptionalBuilder(new ProcessingMessage("pe3"), cause).build(),
        new ValidatorDefaultBuilder().buildException(new ProcessingMessage("pe3"), cause).build()
    );
  }

  @Test
  void buildProcessingMessage() {
    assertEquals(
        new ProcessingMessageOptionalBuilder(new I18nMessage("m1")).build(),
        new ValidatorDefaultBuilder().buildProcessingMessage("m1").build()
    );
    assertEquals(
        new ProcessingMessageOptionalBuilder(new I18nMessage("m2")).build(),
        new ValidatorDefaultBuilder().buildProcessingMessage(new I18nMessage("m2")).build()
    );
  }

  @Test
  void createContextInfo() {
    assertEquals(Optional.of(ContextInfo.of("name", CommonContextValues.DATE)),
        new ValidatorDefaultBuilder().createContextInfo("name", CommonContextValues.DATE));
    assertEquals(
        Optional.of(ContextInfo.of("name", CommonContextValues.TIME, new I18nMessage("msg"))),
        new ValidatorDefaultBuilder()
            .createContextInfo("name", CommonContextValues.TIME, new I18nMessage("msg")));
  }

  @Test
  void createException() {
    assertEquals(Optional.of(new ProcessingException("e1")),
        new ValidatorDefaultBuilder().createException("e1"));
    assertEquals(Optional.of(new ProcessingException("e2")),
        new ValidatorDefaultBuilder().createException(new ProcessingMessage("e2")));
    assertEquals(Optional.of(new ProcessingException("e3")),
        new ValidatorDefaultBuilder().createException(new ProcessingMessage("e3"),
            new Throwable()));
  }

  @Test
  void createMessage() {
    assertEquals(Optional.of(new ProcessingMessage("code-1")),
        new ValidatorDefaultBuilder().createMessage("code-1"));
    assertEquals(Optional.of(new ProcessingMessage("code-2")),
        new ValidatorDefaultBuilder().createMessage(new I18nMessage("code-2")));
  }

  @Test
  void throwException() {
    assertThrows(ProcessingException.class,
        () -> new ValidatorDefaultBuilder().throwException("code"));
    assertThrows(ProcessingException.class,
        () -> new ValidatorDefaultBuilder().throwException(new ProcessingMessage("code")));
    assertThrows(ProcessingException.class,
        () -> new ValidatorDefaultBuilder().throwException(new ProcessingException("code")));
    assertThrows(ProcessingException.class,
        () -> new ValidatorDefaultBuilder().throwException(new ProcessingMessage("a"),
            new Throwable()));
  }
}
