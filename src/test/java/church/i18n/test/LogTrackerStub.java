/*
 * Copyright (c) 2024 Juraj Jurčo
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package church.i18n.test;

import ch.qos.logback.classic.spi.ILoggingEvent;
import java.util.List;
import org.junit.jupiter.api.extension.AfterTestExecutionCallback;
import org.junit.jupiter.api.extension.BeforeTestExecutionCallback;
import org.junit.jupiter.api.extension.ExtensionContext;

public class LogTrackerStub implements BeforeTestExecutionCallback, AfterTestExecutionCallback {

  final LogTracker logTracker = LogTracker.create();

  private LogTrackerStub() {
    //hide the constructor to force the use of the "create" method
  }

  public List<ILoggingEvent> getLoggingEventsForLogLevel(
      final ch.qos.logback.classic.Level logLevel) {
    return logTracker.getLoggingEventsForLogLevel(logLevel);
  }

  public boolean contains(final String loggingStatement) {
    return logTracker.contains(loggingStatement);
  }

  public LogTrackerStub recordForLevel(final LogTracker.LogLevel level) {
    logTracker.recordForLevel(level);
    return this;
  }

  public LogTrackerStub recordForObject(final Object sut) {
    logTracker.recordForObject(sut);
    return this;
  }

  public LogTrackerStub recordForType(final Class<?> type) {
    logTracker.recordForType(type);
    return this;
  }

  public int size() {
    return logTracker.size();
  }

  public static LogTrackerStub create() {
    return new LogTrackerStub();
  }

  @Override
  public void afterTestExecution(final ExtensionContext context) {
    logTracker.resetLoggingFramework();
  }

  @Override
  public void beforeTestExecution(final ExtensionContext context) {
    logTracker.prepareLoggingFramework();
  }

  public List<ILoggingEvent> getLoggingEvents() {
    return logTracker.getLoggingEvents();
  }

}
