/*
 * Copyright (c) 2024 Juraj Jurčo
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package church.i18n.test;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.Logger;
import ch.qos.logback.classic.LoggerContext;
import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.core.read.ListAppender;
import java.util.List;
import java.util.Vector;
import org.slf4j.LoggerFactory;

public class LogTracker {

  private final ListAppender<ILoggingEvent> listAppender = new ListAppender<>();
  private final LoggerContext loggerContext = (LoggerContext) LoggerFactory
      .getILoggerFactory();
  private final Vector<Class<?>> loggingSources = new Vector<>();
  private LogTracker.LogLevel level = LogTracker.LogLevel.TRACE;

  public boolean contains(final String loggingStatement) {
    List<ILoggingEvent> list = listAppender.list;
    for (ILoggingEvent event : list) {
      if (event.getFormattedMessage().contains(loggingStatement)) {
        return true;
      }
    }
    return false;
  }

  public void prepareLoggingFramework() {
    resetLoggingContext();
    addAppenderToLoggingSources();
    listAppender.start();
  }

  public LogTracker recordForLevel(
      final LogTracker.LogLevel level) {
    this.level = level;
    resetLoggingFramework();
    prepareLoggingFramework();
    return this;
  }

  public LogTracker recordForObject(final Object sut) {
    Class<?> type = sut.getClass();
    recordForType(type);
    return this;
  }

  public LogTracker recordForType(final Class<?> type) {
    loggingSources.add(type);
    addAppenderToType(type);
    return this;
  }

  public void resetLoggingFramework() {
    listAppender.stop();
    resetLoggingContext();
  }

  public int size() {
    return listAppender.list.size();
  }

  public List<ILoggingEvent> getLoggingEventsForLogLevel(
      final ch.qos.logback.classic.Level logLevel) {
    return listAppender.list.stream()
        .filter(event -> event.getLevel() == logLevel)
        .toList();
  }

  public static LogTracker create() {
    return new LogTracker();
  }

  private void addAppenderToLoggingSources() {
    for (Class<?> logSource : loggingSources) {
      addAppenderToType(logSource);
    }
  }

  private void addAppenderToType(final Class<?> type) {
    Logger logger = (Logger) LoggerFactory.getLogger(type);
    logger.addAppender(listAppender);
    logger.setLevel(level.internalLevel);
  }

  private void resetLoggingContext() {
    loggerContext.reset();
  }

  public List<ILoggingEvent> getLoggingEvents() {
    return List.copyOf(listAppender.list);
  }

  public enum LogLevel {
    TRACE(Level.TRACE),
    DEBUG(Level.DEBUG),
    INFO(Level.INFO),
    WARN(Level.WARN),
    ERROR(Level.ERROR);

    final Level internalLevel;

    LogLevel(final Level level) {
      internalLevel = level;
    }
  }

}
