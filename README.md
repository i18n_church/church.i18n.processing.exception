# Exception Handling library

Instead of:

```java
throw new IllegalArgumentException("The e-mail address " + email + " is invalid.");
```

Throw:

```java
throw new ProcessingException("err-format-email", email);
```

```properties
err-format-email="The e-mail address {0} is invalid."
```

**Under the hood:**

* Has an error message code (err-format-email)
* Supports formatting
* The message could be localized
* Eco-friendly — could be recycled
* The code is cleaner
* IDE suggestion support

---

`ProcessingException` extends `RuntimeException` and adds extra properties that could lead to a more
standardized way of throwing exceptions and handling important information. Usually, at the place
where exception is thrown, the context is the richest and could provide the most detailed
information about the situation. Once this context is lost, there is no way to retrieve it back.
`RuntimeException` was designed to be generic so everyone can extend it and make its own subclasses.
However, to keep exception handling consistent, one needs a consistent approach to how the base
exception looks like. Otherwise, we need to write another code that will transform our inconsistent
exceptions into a consistent response. The main motivation comes from Spring REST projects, where
you need to keep a standard response structure when communicating with a client.

The library provides a generic model for handling exceptions. It tries to abstract this problem from
the implementation and provides a universal mechanism of dealing with exceptions. The library offers
the following functionalities:

* Defines a generic exception from which other exceptions may extend (if needed)
* Adds support for a *Message Storage* so the program may continue when some validations fail
* Introduces a security concept of information held by the exception
* Differentiates between logging and presenting an error to the client
* Adds support for localization
* Separates context values from messages and adds more details to each value
* Adds information on how to resolve issues
* Involves a "*Processing Status*" which partially collides with an HTTP status
* Includes a standard processing ID (correlation / request ID) for traceability throughout logs and
  responses
* Adds support for easier manipulation with messages and exceptions during validation

## Library focus

### Logging

Logging enough (and not more) information is invaluable for investigation. The place where an
exception is thrown in the code is usually the most information-rich. The more information we
provide in a thrown exception, the easier the further investigation could be. The library adds a
unified structure within the exception so that the one who throws the exception could already add
all information to the base exception.

### Supportability

Tell user how to resolve problem. To help the consumer investigate the cause of the problem, you may
need to add additional information and point them in a good direction. It may reduce support calls.

### Security

The exception may contain both – less and more sensitive information. You may need to restrict
certain information in different contexts – to log a file, through API, to a user or so... The
library allows you to mark information by security levels and handle them later in the code
dynamically as needed.

### Automatization

The consumer needs to react on error states; across releases. The library provides a unified way of
exceptions with their unique error codes, so that the resulting exception could be uniquely
identifiable.

### Localization

Localizing a web page but not error messages? Working with *ResourceBundle*s and provides the
ability to translate messages and their context.

## Simple exception example

### Exception:

A simple validation of an e-mail address:

```java 
new ProcessingException("err-format-email", email)
    .withMessageType(MessageSeverity.HIGH)
    .withStatus(ClientError.BAD_REQUEST)
    .withSecurityLevel(SecurityLevel.SYSTEM_EXTERNAL)
    .withLogLevel(LogLevel.ERROR)
    .withHelpUri("https://tools.ietf.org/html/rfc5322")
    .addContextInfo(
        ContextInfo.of("email-value")
            .withContext("wrong@@email.com", ValueType.STRING)
            .withHelp("^[^@\\s]+@[^@\\s\\.]+\\.[^@\\.\\s]+$", ValueType.REGEXP)
            .withMessage("err-format-email-ctx", email, 7)
            .withSecurityLevel(SecurityLevel.PERSONAL_INFORMATION)
            .build()
    );
```

```properties
err-format-email=The e-mail address {0} is invalid.
err-format-email-ctx=E-mail address ''{0}'' contains invalid character at position [{1}].
```

> Here I acknowledge that the value you use as a status may collide with the actual values of a HTTP
> response status.

Someone may point out that it does not logically belong there and I'm mixing two different contexts.
However, this status code is not strictly bounded to a HTTP status. However, for the REST-based
application, this definition right at the place where the exception is created brings a lot of peace
in handling error states. It makes the code much more readable, error-prone and unambiguous for most
of the library consumers. For those who work with other than HTTP-based communication, you are free
to use a different status value in which your system has occurred in the time of processing.

#### Error log:

```log
2021/05/13 22:47:03,938 [ERROR ] [main] church.i18n.processing.logger.LogLevelLogMapper: err-format-email
ProcessingException{status=BAD_REQUEST, logLevel=ERROR, processingMessage=ProcessingMessage{
                    message=I18nMessage{code='err-format-email', messageParams=[info@@i18n.church]}, 
                    helpUri=https://tools.ietf.org/html/rfc5322, messageType=HIGH, 
                    contextInfo=[ContextInfo{name='email-value', context=ContextValue{value='wrong@@email.com', valueType='string'}, 
                    help=ContextValue{value='^[^@\s]+@[^@\s\.]+\.[^@\.\s]+$', valueType='regexp'}, 
                    message=I18nMessage{code='err-format-email-ctx', messageParams=[info@@i18n.church, 7]}, 
                    securityLevel=PERSONAL_INFORMATION}], securityLevel=SYSTEM_EXTERNAL}} 
	at church.i18n.exception.example.Playground.exampleNewProcessingExceptionWithParameters(Playground.java:106)
	at church.i18n.exception.example.Playground.main(Playground.java:166)
```

## Other code examples

### Re-throwing exception

```java
public void validateToken(final String authToken){
  try{
    Jwts.parser().setSigningKey(tokenKey.getEncoded()).parseClaimsJws(authToken);
  }catch(SignatureException ex){
    throw new ProcessingException("err-a-6",ex).withStatus(ClientError.UNAUTHORIZED);
  }catch(MalformedJwtException ex){
    throw new ProcessingException("err-a-7",ex).withStatus(ClientError.UNAUTHORIZED);
  }catch(ExpiredJwtException ex){
    throw new ProcessingException("err-a-8",ex).withStatus(ClientError.UNAUTHORIZED);
  }catch(UnsupportedJwtException ex){
    throw new ProcessingException("err-a-9",ex).withStatus(ClientError.UNAUTHORIZED);
  }catch(IllegalArgumentException ex){
    throw new ProcessingException("err-a-10",ex).withStatus(ClientError.UNAUTHORIZED);
  }
}
```

```properties
err-a-6=Verification of an existing signature of a token has failed.
err-a-7=Incorrect token structure.
err-a-8=Token has already expired.
err-a-9=Malformed token content.
err-a-10=Token claims are empty.
```

### Validation errors

When the endpoint receives multiple fields (e.g. from a web form), it is more user friendly to
return all validation errors at once so the user may go through them and fix them all rather than to
click 'Submit' for each field separately. Validation errors could be returned as separate messages
in response or as a part of context info, depending on details you need to provide.

Validation errors using a *Message Storage*:

```java
  String username = ""; boolean strongPassword = false; int age = 10; int MIN_AGE = 14;
  when(username, String::isBlank)
      .buildProcessingMessage("err-username-empty")
      .addToMessageStorage(idProvider, messageStorage);
  when(!strongPassword)
      .buildProcessingMessage("err-weak-password")
      .addToMessageStorage(idProvider, messageStorage);
  when(age, a -> a < MIN_AGE)
      .buildProcessingMessage("err-age-too-young", MIN_AGE)
      .addContextInfo(ContextInfo.of("age", new ContextValue(age, ValueType.INTEGER)))
      .addToMessageStorage(idProvider, messageStorage);
  
  List<ProcessingMessage> processingMessages = messageStorage
      .get(idProvider.getProcessingId());
  
  when(processingMessages, Predicate.not(List::isEmpty))
      .buildException("err-form-contains-errors")
      .withStatus(ClientError.BAD_REQUEST)
      .throwException();
```

Validation errors as a *Context Info*:

```java
  when(email, Objects::isNull)
    .buildException("err-format-email", email)
    .withHelpUri("https://tools.ietf.org/html/rfc5322")
    .withSecurityLevel(SecurityLevel.SYSTEM_EXTERNAL)
    .withLogLevel(LogLevel.ERROR)
    .withStatus(ClientError.BAD_REQUEST)
    .addContextInfo(
      when(email, Objects::nonNull)
        .buildContextInfo("email-value")
        .withContext("wrong@@email.com", ValueType.STRING)
        .withHelp("^[^@\\s]+@[^@\\s\\.]+\\.[^@\\.\\s]+$", ValueType.REGEXP)
        .withMessage("err-format-email-ctx", email, 7)
        .withSecurityLevel(SecurityLevel.SYSTEM_INTERNAL)
        .build()
    )
    .throwException();
```

### Message Storage

When the processing of a request is OK, however together with data you want to return a message or
messages, you can store them in a message storage. Later, when returning a response, you may gather
them and return with data. Here is an example on how to build them conditionally and add them to the
storage.

```java
when(runningOutOfSpace)
  .buildProcessingMessage("disk-space", available, total)
  .withMessageType(MessageStatus.WARNING)
  .withHelpUri("https://somestorage.com/kb/disk-space-threshold")
  .addContextInfo(
    ContextInfo.of("disk-space-total").withContext(total)
      .withHelp("https://somestorage.com/plans/buy-storage", "URL")
      .withMessage("disk-space-total", total).build(),
    ContextInfo.of("disk-space-available").withContext(available)
      .withHelp("https://somestorage.com/plans/buy-storage", "URL")
      .withMessage("disk-space-available", available).build(),
    ContextInfo.of("disk-space-threshold").withContext(threshold)
      .withHelp("https://somestorage.com/profile/settings/disk-space-alert", "URL")
      .withMessage("disk-space-threshold", threshold).build()
  )
  .addToMessageStorage(idProvider, messageStorage);
```

```properties
disk-space=Available free space is {0}, below threshold {1}.
disk-space-total=Total disk space is {0}.
disk-space-available=Available disk space is {0}.
disk-space-threshold=Configured alert threshold is {0} MB.
```

### Security

Security levels are customizable and default ones defined in `SecurityLevel` enum are following:

* **PUBLIC**: Publicly known information about the system.
* **SYSTEM_EXTERNAL**: The user has the ability to get to know this information with some efforts.
* **SYSTEM_INTERNAL**: Exposing the internal state or behavior of the system.
* **THIRD_PARTY**: An external library message with no control over its content.
* **PERSONAL_INFORMATION**: Information directly or indirectly identifying an individual.
* **CONFIDENTIAL**: Sensitive information accessed only on limited and need-to-know basis.
* **RESTRICTED**: Highly sensitive information that should not be accessed unless necessary.

### Status values

The HTTP status code represents a generic state of the server for a particular request. As this code
is an overall status, it unifies all errors that may occur at any layer of the multi-layer system
architecture. Some of them represents the network layer (429 Too Many Requests), others request
validity (411 Length Required), others are part of the controller layer (405 Method Not Allowed).
However, some of them also belong to the business / data layer, and those are more directly related
to exceptions thrown by the application. Status codes defined by HTTP/1.1 standard that may be
related to the business / data layer are as follows:

**Client Errors (4xx)**

* 400 Bad Request
* 401 Unauthorized
* 402 Payment Required
* 403 Forbidden
* 404 Not Found
* 409 Conflict
* 410 Gone
* 415 Unsupported Media Type
* 451 Unavailable For Legal Reasons

**Server errors (5xx)**

* 500 Internal Server Error
